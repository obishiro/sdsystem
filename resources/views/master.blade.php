<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('easyui/themes/bootstrap/easyui.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::to('easyui/themes/icon.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('easyui/themes/color.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ URL::to('css/app.css') }}">
 		<link rel="stylesheet" type="text/css" href="{{ URL::to('easyui/themes/color.css') }}">
       
        @yield('css')
		<script type="text/javascript" src="{{ URL::to('easyui/jquery.min.js?='.str_random(8).'')}}"></script>
		<script type="text/javascript" src="{{ URL::to('easyui/jquery.easyui.min.js?='.str_random(8).'')}}">
        </script>
        <script type="text/javascript" src="{{ URL::to('easyui/jquery.maskedinput.min.js?='.str_random(8).'')}}">
        </script>
        
       
    </head>
    <body>
        @if(Auth::check())
        {{-- @include('frontend.menu') --}}
        @endif
    	@yield('content')

        <script type="text/javascript" >
        var URL = "<?php echo URL::to('/');?>";
        // $.ajaxSetup({
        // headers: {
        //    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //             });
        
        </script>
         
        @yield('script') 
    </body>
</html>
