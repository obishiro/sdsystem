{{--     <head><script  src="{{ URL::to('js/form_sd44.js')}}" type="text/javascript"></script></head> --}}
    
    <div id="menu_formsd44" >
        
        <a href="javascript:void(0)" class="easyui-linkbutton c7" data-options="iconCls:'icon-large-loaddata',size:'large',iconAlign:'top'" onclick="formloaddata()">ดึงจากข้อมูลชายไทย</a>
        &nbsp;
        <a href="javascript:void(0)" onclick="saveformsd44()" class="easyui-linkbutton c1" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'">บันทึกข้อมูล</a>
        &nbsp;
        <a href="javascript:void(0)" id="btn_printsd44" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top',disabled:false " onclick="printsd44()">พิมพ์ (แบบ สด.44)</a>
         <a href="javascript:void(0)" id="btn_printsd1" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top',disabled:true" onclick="printsd1()">พิมพ์ (แบบ สด.1)</a>
        <a href="javascript:void(0)" id="btn_printsd9" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top',disabled:true" onclick="printsd9()">พิมพ์ (แบบ สด.9)</a>
         &nbsp;
        <a href="javascript:void(0)" id="btn_printsendbook" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top',disabled:true" onclick="printkadee()">พิมพ์ส่งตัวดำเนินคดีฯ</a>
        <a href="javascript:void(0)" id="btn_printsd35" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top',disabled:true" onclick="printsd35()">พิมพ์หมายเรียกฯ</a> 
          {{--  &nbsp;
        <a href="javascript:void(0)" onclick="clearformsd44()" class="easyui-linkbutton" data-options="iconCls:'icon-large-error',size:'large',iconAlign:'top'">ล้างข้อมูล</a> --}}

   </div>
     <h1></h1>
    <form id="form_sd44" method="post">
    {!! csrf_field() !!}
   <div class="col-4" style="margin-left:5px">
        <div class="easyui-panel" title="ประวัติทหารกองเกิน" style="width:100%;padding:5px;">
           <div style="margin-bottom:2px" class="col-2">
                <label for="">วัน</label>
                 <input class="easyui-combobox" name="date_regis" id="date_regis" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เดือน </label>
                 <input class="easyui-combobox" name="month_regis" id="month_regis" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-3">
                <label for="">ปีลงบัญชี</label>
                 <input class="easyui-textbox" name="year_regis" id="year_regis" style="width:100%" data-options="required:true" value="<?php echo date('Y')+543;?>">
          </div>
            <div style="margin-bottom:2px" class="col-12">
                <label for="">เลขบัตรประจำตัวประชาชน</label>
                <input  name="pid" id="pid" style="width:95%" 
                <?php if (isset($_GET['pid'])) { 
                  echo "value=".$_GET['pid']."";
                }
                ?>
                >
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">ชื่อ</label>
                <input class="easyui-textbox" name="name" id="name" style="width:100%" data-options="required:true">
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">นามสกุล</label>
                <input class="easyui-textbox" name="lname" id="lname" style="width:100%" data-options="required:true">
            </div>
             <div style="margin-bottom:2px" class="col-2">
                <label for="">วัน</label>
                 <input class="easyui-combobox" name="date_dob" id="date_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เดือน </label>
                 <input class="easyui-combobox" name="month_dob" id="month_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-3">
                <label for="">ปีเกิด</label>
                 <select class="easyui-combobox" name="year_dob" id="year_dob" style="width:100%" data-options="required:true">
                    
                </select>
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">อายุ (ปี)</label>
                <input class="easyui-numberbox" name="age" id="age" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">ปีนักษัตร</label>
               <input class="easyui-combobox" name="zodiac" id="zodiac" style="width:100%" data-options="required:true">
                   
            </div>
             <div style="margin-bottom:2px" class="col-12">
                <label for="">ตำหนิ (แผลเป็น)</label>
                <input class="easyui-combobox" name="scar" id="scar" style="width:95%" data-options="required:true">
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">สัญชาติ</label>
                 <select class="easyui-combobox" name="nationality" style="width:100%" data-options="required:true">
                    <option value="1">ไทย</option>
                </select>
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">ศาสนา</label>
                 <input class="easyui-combobox" name="religion" id="religion" style="width:100%" data-options="required:true">
                 
            </div>
            <div style="margin-bottom:2px" class="col-12">
                <label for="">ระดับการศึกษา</label>
                 <input class="easyui-combobox" name="education" id="education" style="width:95%" data-options="required:true">
                  
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">อาชีพ</label>
                 <input class="easyui-combobox" name="occupation" id="occupation" style="width:95%" data-options="required:true">
                     
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">เบอร์โทรศัพท์</label>
                <input name="phone" id="phone" style="width:100%" data-options="required:true">
            </div>
          
                
        </div>
    </div>
    
    <div class="col-4" style="margin-left:5px">
  <div class="easyui-panel" title="ข้อมูลบิดา มารดา" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-12">
                <label for="">ลงบัญชีทหารตามภูมิลำเนา</label>
                 <input class="easyui-combobox" name="regisfollow" id="regisfollow" style="width:95%" data-options="required:true">
                     
        </div>
        <div style="margin-bottom:2px" class="col-12">
            <label for="">บิดาสัญชาติ</label>
                <select class="easyui-combobox" name="fa_nationality" style="width:95%" data-options="required:true">
                    <option value="1">ไทย</option>
                </select>
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ชื่อบิดา</label>
                <input class="easyui-textbox" name="fa_name" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">นามสกุลบิดา</label>
                <input class="easyui-textbox" name="fa_lname" id="fa_lname" style="width:100%" data-options="required:true">
        </div>

        <div style="margin-bottom:2px" class="col-5">
                <label for="">ชื่อมารดา</label>
                <input class="easyui-textbox" name="ma_name"  style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">นามสกุลมารดา</label>
                <input class="easyui-textbox" name="ma_lname" id="ma_lname" style="width:100%" data-options="required:true">
        </div>

  </div>
   <div style="height:5px;"></div>
  <div class="easyui-panel" title="ภูมิลำเนาทหาร" style="width:100%;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ถนน</label>
                <input class="easyui-textbox" name="road" value="-" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ตรอก</label>
                <input class="easyui-textbox" name="trok" value="-" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ซอย</label>
                <input class="easyui-textbox" name="soy" value="-" style="width:100%" data-options="required:true">
        </div>
       
        <div style="margin-bottom:2px" class="col-5">
                <label for="">บ้านเลขที่</label>
                <input class="easyui-textbox" name="homeid" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">หมู่ที่</label>
                <select id="moo" class="easyui-combobox" name="moo" required="true" style="width:100%;">
                <option value=""></option>
                @for($i=1;$i<=30;$i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ตำบล/แขวง</label>
                <select   name="district" id="district" style="width:100%" data-options="required:true"></select>
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">อำเภอ/เขต</label>
                <input class="easyui-combobox" name="aumphur" id="aumphur" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">จังหวัด</label>
                 <input class="easyui-combobox" name="province" id="province" style="width:100%" data-options="required:true">
        </div>

  </div>
    </div>
        <div class="col-4" style="margin-left:5px">
        <div class="easyui-panel" title="ข้อมูลบัญชีทหารกองเกิน (แบบ สด.๑)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
             <label for="">(แบบ สด.๑) เลขที่</label>
                <input class="easyui-numberbox" name="sd1_no" id="sd1_no" style="width:100%">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ใบสำคัญ (แบบ สด.๙) เลขที่</label>
                <input class="easyui-numberbox" name="sd9_no" id="sd9_no" style="width:100%" data-options="required:true">
        </div>
         <div style="margin-bottom:2px" class="col-12">
                <label for="">ใบสำคัญ (แบบ สด.๙) เลมที่</label>
                <input class="easyui-numberbox" name="sd9_book" id="sd9_book" style="width:95%" >
        </div>
        </div>
    </div>
     <div class="col-4" style="margin-left:5px">
        <div class="easyui-panel" title="ข้อมูลบัตรประจำตัวประชาชน" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-12">
                <label for="">บัตรประจำตัวประชาชนเลขที่</label>
                <input   name="pcard_id" id="pcard_id" style="width:95%" >
        </div>
        <div style="margin-bottom:2px" class="col-12">
                <label for="">ออกให้ ณ ที่</label>
                <input class="easyui-textbox" name="pcard_by" value="ที่ว่าการอำเภอ{{ $office->AMPHUR_NAME}}" style="width:95%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-12">
              <div style="margin-bottom:2px" class="col-2">
                <label for="">วัน</label>
                 <input class="easyui-combobox" name="date_issue" id="date_issue" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เดือน </label>
                 <input class="easyui-combobox" name="month_issue" id="month_issue" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-3">
                <label for="">ปีที่ออกบัตร</label>
                 <input class="easyui-textbox" name="year_issue" id="year_issue" style="width:100%" data-options="required:true" value="<?php echo date('Y')+543;?>">
          </div>
        </div>
        </div>
    </div>

     <div class="col-4" style="margin-left:5px">
        <div class="easyui-panel" title="เลขที่คดี ม.๑๘ / เจ้าหน้าที่รับลงบัญชีฯ " style="width:100%;padding:10px;">
       <div style="margin-bottom:2px" class="col-5" id="kadee_box">
                <label for="">เลขที่คดี ม.๑๘</label>
               <input class="easyui-textbox" name="kadee" id="kadee" style="width:95%" >
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">เจ้าหน้าที่รับลงบัญชีฯ</label>
               <input class="easyui-combobox" name="position" id="position" style="width:95%" data-options="required:true">
                    
        </div>
        </div>
    </div>
   
    </form>
    <script type="text/javascript">
    $("#kadee_box").css("display", "none");
   
$('#pid').textbox({ 
  required:true,
    onChange: function(pid){
         $.ajax({
            type: "POST",
            url: URL+'/checkpid',
            data:{'pid':pid},
            success:function(data){
                var obj = jQuery.parseJSON(data);
                if(obj.status>0)
                {
                    $.messager.alert('ผลการทำงาน', "บุคคลผู้นี้ได้แสดงตนเพื่อลงบัญชีทหารแล้ว", 'error');
                    /* $('#main_layout').panel({
                     href:URL+'/form/sd44'
                    });*/
                }
                
            }
            });
    }

  }).textbox('textbox').mask("9-9999-99999-99-9",{placeholder:" "});
$('#phone').textbox({  required:true}).textbox('textbox').mask("999-999-9999",{placeholder:" "});
$('#lname').textbox({
        onChange:function(res){
          $('#fa_lname').textbox({value:res});
          $('#ma_lname').textbox({value:res});
        }
});
$('#date_dob').combobox({
        url:URL+'/data/date',
        valueField:'id',
        textField:'text'
});
$('#month_dob').combobox({
        url:URL+'/data/month',
        valueField:'id',
        textField:'text'
});
$('#date_regis').combobox({
        url:URL+'/data/date_regis',
        valueField:'id',
        textField:'text'
});
$('#month_regis').combobox({
        url:URL+'/data/month_regis',
        valueField:'id',
        textField:'text'
});
$('#date_issue').combobox({
        url:URL+'/data/date_issue',
        valueField:'id',
        textField:'text'
});
$('#month_issue').combobox({
        url:URL+'/data/month_issue',
        valueField:'id',
        textField:'text'
});
$('#year_dob').combobox({
        url:URL+'/data/year',
        valueField:'id',
        textField:'text',
        onSelect:function(rec){
        // var d = new Date();
        // var n = d.getFullYear(); 
        // var y = n+543;
        var y = $('#year_regis').val();
        var age = y - rec.id ;
        
           $('#age').numberbox({
                value:age
            });
           $('#zodiac').combobox({
                url:URL+'/datazodiac/'+rec.id,
                valueField:'zodiac_no',
                textField:'zodiac_name',
           });
           if(age> 17)
           {
            //$.messager.alert('ผลการทำงาน', "บุคคลผู้นี้แสดงตนตามมาตรา 18 \n กรุณาอย่าลืมกรอกเลขที่คดีด้วย", 'info');

            $("#kadee_box").show();
            $("#kadee").textbox({
                required: true
            });
           }else if(age == 17){
            $("#kadee_box").css("display", "none");
           }else if (age < 17){
            $.messager.alert('ผลการทำงาน', "บุคคลผู้นี้อายุยังไม่ถึงกำหนดการแสดงตนฯ", 'info');

           }
           
        }
        
});

$('#religion').combobox({
        url:URL+'/data/religion',
        valueField:'religion_no',
        textField:'religion_name'
});
$('#education').combobox({
        url:URL+'/data/education',
        valueField:'education_no',
        textField:'education_name'
});
$('#occupation').combobox({
        url:URL+'/data/occupation',
        valueField:'occupation_no',
        textField:'occupation_name'
});
$('#regisfollow').combobox({
        url:URL+'/data/regisfollow',
        valueField:'rf_no',
        textField:'rf_name'
});
$('#scar').combobox({
        url:URL+'/data/scar',
        valueField:'scar_no',
        textField:'scar_name'
});
$('#district').combobox({
        url:URL+'/data/district',
        valueField:'DISTRICT_CODE',
        textField:'DISTRICT_NAME',
        // onSelect:function(rec){
        //    $('#sd1_no').textbox({
        //     value:res
        //   });
        // }
});
$('#aumphur').combobox({
        url:URL+'/data/aumphur',
        valueField:'AMPHUR_CODE',
        textField:'AMPHUR_NAME'
});
$('#province').combobox({
        url:URL+'/data/province',
        valueField:'PROVINCE_CODE',
        textField:'PROVINCE_NAME'
});
$('#position').combobox({
        url:URL+'/data/position',
        valueField:'position_no',
        textField:'position_name'
});

$.extend($.fn.datebox.defaults,{
    formatter:function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
    },
    parser:function(s){
        if (!s) return new Date();
        var ss = s.split('-');
        var d = parseInt(ss[0],10);
        var m = parseInt(ss[1],10);
        var y = parseInt(ss[2],10);
        if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
            return new Date(y,m-1,d);
        } else {
            return new Date();
        }
    }
});
$('#pcard_id').textbox({  required:true }).textbox('textbox').mask("9999-99-99999999",{placeholder:" "});
$('#sd9_no').numberbox({
    required:true,
        onChange: function(sd9_no){
         $.ajax({
            type: "POST",
            url: URL+'/countbooksd9',
            data:{'number':sd9_no},
            success:function(data){
                var obj = jQuery.parseJSON(data);
                if(obj.status==true)
                {
                   $('#sd9_book').textbox({value:obj.number});
                }
                
            }
            });
    }
});
function printsd44() {
  var pid = $('#pid').val();
 
         $.ajax({
             type: "POST",
             url: URL+'/export/sd44',
             data:{'pid':pid},
             success:function(data){
                 var obj = jQuery.parseJSON(data);
                 if(obj.status=='0')
                 {
                   $.messager.alert('ผลการทำงาน', "บุคคลนี้ยังไม่เคยแสดงตนเพื่อลงบัญชีทหาร", 'info');
                 }else{
                  $("#form_sd44").attr("action", URL+"/export/sd44" );
                  $("#form_sd44").submit();
                 }
                
             }
             });
   
}
 function printsd1() {
   var pid = $('#pid').val();
 
          $.ajax({
              type: "POST",
              url: URL+'/export/sd1',
              data:{'pid':pid},
              success:function(data){
                  var obj = jQuery.parseJSON(data);
                  if(obj.status=='0')
                  {
                    $.messager.alert('ผลการทำงาน', "บุคคลนี้ยังไม่เคยแสดงตนเพื่อลงบัญชีทหาร", 'info');
                  }else{
                   $("#form_sd44").attr("action", URL+"/export/sd1" );
                  document.getElementById("form_sd44").submit();
                  }
                
              }
              });
   
 }
 function printsd9() {
   var pid = $('#pid').val();
 
          $.ajax({
              type: "POST",
              url: URL+'/export/sd1',
              data:{'pid':pid},
              success:function(data){
                  var obj = jQuery.parseJSON(data);
                  if(obj.status=='0')
                  {
                    $.messager.alert('ผลการทำงาน', "บุคคลนี้ยังไม่เคยแสดงตนเพื่อลงบัญชีทหาร", 'info');
                  }else{
                   $("#form_sd44").attr("action", URL+"/export/sd9" );
                  document.getElementById("form_sd44").submit();
                  }
                
              }
              });
   
 }
function formloaddata () {
  var pid = $('#pid').val();
  if(pid!="")
  {
        $.ajax({
             type: "POST",
             url: URL+'/checkthaiperson',
             data:{'pid':pid},
             success:function(data){
                 var obj = jQuery.parseJSON(data);
                 if(obj.status=='0')
                 {
                   $.messager.alert('ผลการทำงาน', "ไม่มีข้อมูลบุคคลนี้ในบัญชีข้อมูลชายไทย", 'error');
                  // $('#form_sd44').form('clear');
                 }else{
                   $('#form_sd44').form('load', URL+'/data0/'+pid);
             }
        }                  
         });
   
 //   $('#form_sd44').form('load', URL+'/data0/'+pid);
  }else{

     $.messager.alert('ผลการทำงาน', "กรุณากรอกเลขบัตรประจำตัวประชาชนด้วย!", 'error');
  }
  }
 
 
    </script>
 