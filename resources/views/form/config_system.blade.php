 <div id="menu_formsd44" >
         
        <a href="javascript:void(0)" onclick="saveconfigsystem()" class="easyui-linkbutton" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" >บันทึกข้อมูล</a>
        &nbsp;&nbsp;
        <a href="javascript:void(0)" onclick="clearformconfigsystem()" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',size:'large',iconAlign:'top'">ล้างข้อมูล</a>
   </div>
<form id="form_configsystem" method="post">
	<div class="col-5">
        <div class="easyui-panel" title="ตั้งค่าข้อมูลพื้นฐาน" style="width:100%;padding:5px;">
        	<div style="margin-bottom:2px" class="col-5">
                <label for="">นามหน่วย (ตัวเต็ม)</label>
                 <input class="easyui-textbox" name="office_name" value="{{ $office_name }}" id="date_regis" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">นามหน่วย (ตัวย่อ)</label>
                 <input class="easyui-textbox" name="office_shortname" value="{{ $office_shortname}}" id="office_shortname" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เลขที่หนังสือส่วนราชการ (มท.)</label>
                 <input class="easyui-textbox" name="office_no" id="office_no" value="{{ $office_no }}" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เลขที่หนังสือส่วนราชการ (กห.)</label>
                 <input class="easyui-textbox" name="office_soldier_no" value="{{ $office_soldier_no}}" id="office_soldier_no" style="width:100%" data-options="required:true">
            </div>
             <div style="margin-bottom:2px" class="col-5">
                <label for="">ถนน</label>
                <input class="easyui-textbox" name="road" value="{{ $road }}" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ตรอก</label>
                <input class="easyui-textbox" name="trok" value="{{ $trok}}" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ซอย</label>
                <input class="easyui-textbox" name="soy" value="{{$soy }}" style="width:100%" data-options="required:true">
        </div>
       
        <div style="margin-bottom:2px" class="col-5">
                <label for="">เลขที่</label>
                <input class="easyui-textbox" name="office_home_id" value="{{ $office_home_id }}" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">หมู่ที่</label>
                <input class="easyui-numberbox" name="office_moo" value="{{ $office_moo}}" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ตำบล/แขวง</label>
                <input class="easyui-textbox" name="office_district" id="office_district" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">อำเภอ/เขต</label>
                <input class="easyui-combobox" name="office_amphur" id="office_amphur" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">จังหวัด</label>
                 <input class="easyui-combobox" name="office_province" id="office_province" style="width:100%" data-options="required:true">
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">หมายเลขโทรศัพท์</label>
                 <input   name="office_tel"  id="office_tel" value="{{ $office_tel}}" style="width:100%" >
            </div>
        </div>
    </div>
    <div class="col-5">
        <div class="easyui-panel" title="วัน เดือน ปี ที่ทำการตรวจเลือกฯ " style="width:100%;padding:10px;">
     <div style="margin-bottom:2px" class="col-2">
                <label for="">วัน</label>
                 <input class="easyui-combobox" name="date_dob" id="date_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เดือน </label>
                 <input class="easyui-combobox" name="month_dob" id="month_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-3">
                <label for="">ปี พ.ศ.</label>
                 <input class="easyui-textbox" name="year_regis" id="year_regis" style="width:100%" data-options="required:true">
          </div>  
          <div style="margin-bottom:2px" class="col-12">
               <label for="">กรอกสถานที่ทำการตรวจเลือกฯ  </label>
               <br> 
              <input class="easyui-textbox" name="place" id="place" style="width:100%" data-options="required:true" value="">
                    
        </div>       
</div>
    
</form>
<script type="text/javascript">
// $('#office_district').combobox({
//         url:URL+'/data/alldistrict',
//         valueField:'DISTRICT_CODE',
//         textField:'DISTRICT_NAME'
// });
$('#date_dob').combobox({
        url:URL+'/data/date',
        valueField:'id',
        textField:'text'
});
$('#month_dob').combobox({
        url:URL+'/data/month',
        valueField:'id',
        textField:'text'
});
 
// $('#office_amphur').combobox({
        
//          onSelect:function(rec){
//              $('#office_district').combobox({
//             url:URL+'/datadistrict/'+rec.AMPHUR_ID,
//             valueField:'DISTRICT_CODE',
//             textField:'DISTRICT_NAME'
//             });
//         }
// });
// $('#office_province').combobox({
//         url:URL+'/data/allprovince',
//         valueField:'PROVINCE_ID',
//         textField:'PROVINCE_NAME',
//         onSelect:function(rec){
//              $('#office_amphur').combobox({
//             url:URL+'/dataamphur/'+rec.PROVINCE_ID,
//             valueField:'AMPHUR_ID',
//             textField:'AMPHUR_NAME'
//             });
//         }
// });
$('#district').combobox({
        url:URL+'/data/district',
        valueField:'DISTRICT_CODE',
        textField:'DISTRICT_NAME',
        // onSelect:function(rec){
        //    $('#sd1_no').textbox({
        //     value:res
        //   });
        // }
});
$('#office_amphur').combobox({
        url:URL+'/data/aumphur',
        valueField:'AMPHUR_CODE',
        textField:'AMPHUR_NAME'
});
$('#office_province').combobox({
        url:URL+'/data/province',
        valueField:'PROVINCE_CODE',
        textField:'PROVINCE_NAME'
});
$('#office_tel').textbox({  required:true }).textbox('textbox').mask("9-9999-9999",{placeholder:" "});
function saveconfigsystem() {
         $('#form_configsystem').form('submit',{
            url: URL+'/save/configsystem',
            cache:false,
            data:$('#form_configsystem').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลเรียบร้อยแล้ว!", 'info');
                

            } 
          
           
        }
    });
}
</script>
 
