 <div style="margin:5px;">
 	<div id="menu_formsd44" >
	  <a href="javascript:void(0)" onclick="exportsd35()" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top'">ส่งออกหมายเรียกประจำปี</a>
         &nbsp;&nbsp;
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-large-printer',size:'large',iconAlign:'top'" onclick="exportreportsd35()">ส่งออกงบหน้าหมายเรียกฯ</a>
        &nbsp;&nbsp;
 	</div>
  <form method="POST" id="form_sd35" name="form_sd35" action="#">
 <div class="col-4" >
        <div class="easyui-panel" title="หมายเรียกฯ สำหรับคนเกิด พ.ศ./ชั้นปี " style="width:100%;padding:10px;">
        	   <div style="margin-bottom:2px" class="col-5">
                <label for="">เลือกปีเกิด / ชั้นปี</label>
                  <select    name="year_dob" id="year_dob" style="width:100%"  data-options="required:true">
 				{{ Helpers::GetYear()}}
				</select>
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">ตำบล </label>
                <select  name="district" id="district" style="width:100%" data-options="required:true">
       {{ Helpers::GetDistrict()}}
  </select>
            </div>
             
      
        </div>

    </div>
<div class="col-4" >
<div class="easyui-panel" title="วัน เดือน ปี ที่ทำการตรวจเลือกฯ " style="width:100%;padding:10px;">
     <div style="margin-bottom:2px" class="col-2">
                <label for="">วัน</label>
                 <input class="easyui-combobox" name="date_dob" id="date_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-5">
                <label for="">เดือน </label>
                 <input class="easyui-combobox" name="month_dob" id="month_dob" style="width:100%" data-options="required:true">
            </div>
            <div style="margin-bottom:2px" class="col-3">
                <label for="">ปี พ.ศ.</label>
                 <input class="easyui-textbox" name="year_regis" id="year_regis" style="width:100%" data-options="required:true">
          </div>       	 
</div>
 </div>
 <div class="col-3" >
        <div class="easyui-panel" title="สถานที่ทำการตรวจเลือกฯ " style="width:100%;padding:10px;">
        	 <div style="margin-bottom:2px" class="col-12">
               <label for="">กรอกสถานที่ทำการตรวจเลือกฯ  </label>
               <br>	
              <input class="easyui-textbox" name="place" id="place" style="width:100%" data-options="required:true" value="">
                    
        </div>
        </div>

    </div>
    </form>


 </div>
 <script type="text/javascript">
$('#date_dob').combobox({
        url:URL+'/data/date',
        valueField:'id',
        textField:'text'
});
$('#month_dob').combobox({
        url:URL+'/data/month',
        valueField:'id',
        textField:'text'
});
$('#year_dob').combobox({
	onChange:function(res){
			var year = Number(res) + 21;
          $('#year_regis').textbox({value:year});
           
        }
});
    $('#district').combobox({
        url:URL+'/data/district',
        valueField:'DISTRICT_CODE',
        textField:'DISTRICT_NAME'
});
function exportsd35()
{
	var year_dob = $('#year_dob').combobox('getValue');
  var district = $('#district').combobox('getValue');
  var date_dob = $('#date_dob').combobox('getValue');
  var month_dob = $('#month_dob').combobox('getValue');
//  var year_regis = $('#year_regis').combobox('getValue');
 var place = $('#place').textbox('getValue');
  if (year_dob=="- เลือกปีเกิด/ชั้นปี -" || district=="- เลือกตำบล -" || date_dob=="" || month_dob==""  || place=="")
  {
    $.messager.alert('ผลการทำงาน', "กรุณาเลือก/กรอกข้อมูลให้ครบทุกช่องด้วย", 'error');
  }else{
  $("#form_sd35").attr("action", URL+"/export/sd35year" );
   document.getElementById("form_sd35").submit();
 }
}
function exportreportsd35()
{
  var year_dob = $('#year_dob').combobox('getValue');
  var district = $('#district').combobox('getValue');
  if (year_dob=="- เลือกปีเกิด/ชั้นปี -" || district=="- เลือกตำบล -")
  {
    $.messager.alert('ผลการทำงาน', "กรุณาเลือกปีเกิด/ตำบลด้วย", 'error');
  }else{
  $("#form_sd35").attr("action", URL+"/export/reportsd35year" );
   document.getElementById("form_sd35").submit();
 
}
}
 </script>