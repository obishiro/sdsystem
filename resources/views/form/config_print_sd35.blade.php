 <div id="menu_formsd44" >
         
        <a href="javascript:void(0)" onclick="saveconfigprintsd35()" class="easyui-linkbutton" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" >บันทึกข้อมูล</a>
        &nbsp;&nbsp;
        <a href="javascript:void(0)" onclick="clearconfigprintsd35()" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',size:'large',iconAlign:'top'">คืนค่าข้อมูลเดิม</a>
   </div>
 
   {!! Form::open(['id'=>'form_saveconfigprintsd35', 'method' => 'post']) !!}
<div class="col-4">
 <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความสูง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 8</label>
               <input class="easyui-textbox" name="row8" id="row8" style="width:95%" data-options="required:true" value="{{ $c->row8}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 10</label>
               <input class="easyui-textbox" name="row10" id="row10" style="width:95%" data-options="required:true" value="{{ $c->row10}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 11</label>
               <input class="easyui-textbox" name="row11" id="row11" style="width:95%" data-options="required:true" value="{{ $c->row11}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 12</label>
               <input class="easyui-textbox" name="row12" id="row12" style="width:95%" data-options="required:true" value="{{ $c->row12}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 13</label>
               <input class="easyui-textbox" name="row13" id="row13" style="width:95%" data-options="required:true" value="{{ $c->row13}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 15</label>
               <input class="easyui-textbox" name="row15" id="row15" style="width:95%" data-options="required:true" value="{{ $c->row15}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 36</label>
               <input class="easyui-textbox" name="row36" id="row36" style="width:95%" data-options="required:true" value="{{ $c->row36}}">
                    
        </div>
        
        </div>
    </div>

     <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความกว้าง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ A</label>
               <input class="easyui-textbox" name="column_a" id="column_a" style="width:95%" data-options="required:true" value="{{ $c->column_a}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ B</label>
               <input class="easyui-textbox" name="column_b" id="column_b" style="width:95%" data-options="required:true" value="{{ $c->column_b}}">
        </div>
         
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ G</label>
               <input class="easyui-textbox" name="column_g" id="column_g" style="width:95%" data-options="required:true" value="{{ $c->column_g}}">            
        </div>
         
         
        </div>
    </div>

  </div>
  </form>
  <div class="col-8">
    <div class="easyui-panel" title="ผังประกอบการตั้งค่า" style="width:100%;padding:10px;">
      <img src="{{ URL::to('images/export_template/sd35.png')}}" width="98%" alt="">
    </div>
  </div>
  <script type="text/javascript">
    function saveconfigprintsd35() {
         $('#form_saveconfigprintsd35').form('submit',{
            url: URL+'/configprint/saveconfigprintsd35',
            cache:false,
            data:$('#form_saveconfigprintsd35').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลเรียบร้อยแล้ว!", 'info');
                $('#main_layout').panel({href:URL+'/configprint/sd35'});
                

            } 
          
           
        }
    });
}
function clearconfigprintsd35() {
    $('#row8').textbox({value:'31.5'});

    $('#row10').textbox({value:'21'});
    $('#row11').textbox({value:'19.5'});
    $('#row12').textbox({value:'27.75'});
    $('#row13').textbox({value:'17.25'});
    $('#row15').textbox({value:'18.75'});

    $('#row36').textbox({value:'36.75'});
    $('#column_a').textbox({value:'11.25'});
    $('#column_b').textbox({value:'6'});
     
    $('#column_g').textbox({value:'13'});
     


}
  </script>

