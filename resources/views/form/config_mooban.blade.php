  <meta name="csrf-token" content="{{ csrf_token() }}">
 <div style="margin:5px;">

    <table id="dg-ban" title="ข้อมูลหมู่บ้าน"  class="easyui-datagrid" style="width:100%;height:500px"
            url="result/mooban"
            toolbar="#toolbar" pagination="true"
            rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
                <th  field="district" width="10%">รหัสตำบล</th>
            	<th field="DISTRICT_NAME" width="20%">ตำบล</th>
                <th field="moo" width="10%">หมู่ที่</th>
                <th field="banname" width="20%">ชื่อหมู่บ้าน</th>
                <th field="bossname" width="20%">ชื่อผู้ใหญ่บ้าน</th>
                <th field="phonenumber" width="20%">เบอร์โทรศัพท์</th>
               
            </tr>
        </thead>
    </table>
    </div>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="newBan()">เพิ่มข้อมูลใหม่</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editBan()">แก้ไขข้อมูล</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyBan()">ลบข้อมูล</a>
    </div>
    
    <div id="dlg-ban" class="easyui-dialog" style="width:500px"
            closed="true" buttons="#dlg-buttons" data-options="modal:true">
       {!! Form::open(['id'=>'fm','style'=>'margin:0;padding:20px 50px', 'method' => 'post']) !!}
  
            
            
            <div style="margin-bottom:10px">
                <input name="district" id="district" class="easyui-combobox" required="true" label="เลือกตำบล/แขวง" style="width:100%">
            </div>
              <div style="margin-bottom:10px">
               <select id="moo" class="easyui-combobox" name="moo" required="true" label="เลือกหมู่ที่"style="width:100%;">
                @for($i=1;$i<=30;$i++)
                <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
            </div>
            <div style="margin-bottom:10px">
                <input name="banname" class="easyui-textbox" required="true"  label="ชื่อหมู่บ้าน" style="width:100%">
            </div>
             <div style="margin-bottom:10px">
                <input name="bossname" class="easyui-textbox" required="true"  label="ชื่อผู้ใหญ่บ้าน" style="width:100%">
            </div>
             <div style="margin-bottom:10px">
                <input name="phonenumber" id="phonenumber"  label="เบอร์โทรศัพท์" style="width:100%">
            </div>
           
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveBan()" >บันทึกข้อมูล</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" onclick="javascript:$('#dlg-ban').dialog('close')" style="width:90px">ยกเลิก</a>
    </div>
     
    <script type="text/javascript">

        var url;
        function newBan(){
            $('#dlg-ban').dialog('open').dialog('center').dialog('setTitle','เพิ่มข้อมูลใหม่');
            $('#fm').form('clear');
            url = URL+'/save/mooban';
        }
        function editBan(){
            var row = $('#dg-ban').datagrid('getSelected');
            if (row){
                $('#dlg-ban').dialog('open').dialog('center').dialog('setTitle','แก้ไขข้อมูล');
                $('#fm').form('load',row);
                url = 'editban/'+row.district+'/'+row.moo;

            }
            
        }
        function saveBan(){
            $('#fm').form('submit',{
               
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                        var result = eval('('+result+')');
                       if (result.success){
                               // $('#dg-ban').datagrid('reload');    // reload the Ban data
                                $.messager.alert({
                                    icon:'info',
                                    title:'แจ้งผลการทำงาน',
                                    msg:result.errorMsg
                                    
                                });
                               
                            } else {
                                $.messager.alert({    // show error message
                                    title: 'Error',
                                    msg: result.errorMsg,
                                    icon:'error'
                                });
                            }
                             $('#dlg-ban').dialog('close');        // close the dialog
                             $('#dg-ban').datagrid('reload');                    
                }
            });
        }
        function destroyBan(){
            var row = $('#dg-ban').datagrid('getSelected');
            if (row){
                $.messager.confirm('ยืนยัน','ต้องการลบข้อมูลจริงหรือไม่ ?',function(r){
                    if (r){
                        $.post('deleteban',{id:row.district,moo:row.moo},function(result){
                            if (result.success){
                                $('#dg-ban').datagrid('reload');    // reload the Ban data
                                 $.messager.alert({
                                   
                                    title:'แจ้งผลการทำงาน',
                                    msg:'ลบข้อมูลเรียบร้อยแล้ว',
                                     icon:'info'
                                    
                                })
                            } else {
                                $.messager.alert({    // show error message
                                    title: 'แจ้งผลการทำงาน',
                                    msg: result.errorMsg
                                });
                            }
                        },'json');
                    }
                });
            }
        }
        
       // $('#moo').combobox({
       //  url:URL+'/data/position',
       //  valueField:'position_no',
       //  textField:'position_name'
       //  });
       
        $('#district').combobox({
        url:URL+'/data/district',
        valueField:'DISTRICT_CODE',
        textField:'DISTRICT_NAME',
        // onSelect:function(rec){
        //    $('#sd1_no').textbox({
        //     value:res
        //   });
        // }
});
        $('#phonenumber').textbox({  required:true}).textbox('textbox').mask("999-999-9999",{placeholder:" "});
    </script>
{{-- http://www.jeasyui.com/forum/index.php?topic=532.0 --}}