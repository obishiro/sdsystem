 <div id="menu_formsd1" >
         
        <a href="javascript:void(0)" onclick="saveconfigprintsd9()" class="easyui-linkbutton" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" >บันทึกข้อมูล</a>
        &nbsp;&nbsp;
        <a href="javascript:void(0)" onclick="clearconfigprintsd9()" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',size:'large',iconAlign:'top'">คืนค่าข้อมูลเดิม</a>
   </div>
 
 
<div class="col-4">
    {!! Form::open(['id'=>'form_saveconfigprintsd9', 'method' => 'post']) !!}
 <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความสูง (Points)" style="width:100%;padding:10px;">
            <div style="margin-bottom:2px" class="col-11">
                <label for="">สด.สำหรับปีเกิด/ชั้นปี</label>
                <br>
               <select class="easyui-combobox"  name="year_dob" id="year_dob" style="width:100%">
                {{ Helpers::GetYear()}}
              </select> 
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 1</label>
               <input class="easyui-textbox" name="row1" id="row1" style="width:95%" data-options="required:true" value="{{ $c->row1}}">
                    
        </div>
       
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 6</label>
               <input class="easyui-textbox" name="row6" id="row6" style="width:95%" data-options="required:true" value="{{ $c->row6}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 8</label>
               <input class="easyui-textbox" name="row8" id="row8" style="width:95%" data-options="required:true" value="{{ $c->row8}}">
                    
        </div>
       
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 18</label>
               <input class="easyui-textbox" name="row18" id="row18" style="width:95%" data-options="required:true" value="{{ $c->row18}}">
                    
        </div>
         
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแต่ละบรรทัด</label>
               <input class="easyui-textbox" name="allrow" id="allrow" style="width:95%" data-options="required:true" value="{{ $c->allrow}}">
                    
        </div>
        </div>
    </div>

     <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความกว้าง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ A</label>
               <input class="easyui-textbox" name="column_a" id="column_a" style="width:95%" data-options="required:true" value="{{ $c->column_a}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ B</label>
               <input class="easyui-textbox" name="column_b" id="column_b" style="width:95%" data-options="required:true" value="{{ $c->column_b}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ C</label>
               <input class="easyui-textbox" name="column_c" id="column_c" style="width:95%" data-options="required:true" value="{{ $c->column_c}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ D</label>
               <input class="easyui-textbox" name="column_d" id="column_d" style="width:95%" data-options="required:true" value="{{ $c->column_d}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ E</label>
               <input class="easyui-textbox" name="column_e" id="column_e" style="width:95%" data-options="required:true" value="{{ $c->column_e}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ F</label>
               <input class="easyui-textbox" name="column_f" id="column_f" style="width:95%" data-options="required:true" value="{{ $c->column_f}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ G</label>
               <input class="easyui-textbox" name="column_g" id="column_g" style="width:95%" data-options="required:true" value="{{ $c->column_g}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ H</label>
               <input class="easyui-textbox" name="column_h" id="column_h" style="width:95%" data-options="required:true" value="{{ $c->column_h}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ I</label>
               <input class="easyui-textbox" name="column_i" id="column_i" style="width:95%" data-options="required:true" value="{{ $c->column_i}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ J</label>
               <input class="easyui-textbox" name="column_j" id="column_j" style="width:95%" data-options="required:true" value="{{ $c->column_j}}">            
        </div>
         
         
        </div>
    </div>

  </div>
  </form>
  <div class="col-8">
    <div class="easyui-panel" title="ผังประกอบการตั้งค่า" style="width:100%;padding:10px;">
      <img src="{{ URL::to('images/export_template/sd9.png')}}" width="98%" alt="">
    </div>
  </div>
  <script type="text/javascript">
    function saveconfigprintsd9() {
         $('#form_saveconfigprintsd9').form('submit',{
            url: URL+'/configprint/saveconfigprintsd9',
            cache:false,
            data:$('#form_saveconfigprintsd9').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลเรียบร้อยแล้ว!", 'info');
                $('#main_layout').panel({href:URL+'/configprint/sd9'});
                

            } 
          
           
        }
    });
}
function clearconfigprintsd9() {
    $('#row1').textbox({value:'84.50'});
    
    $('#row6').textbox({value:'25.75'});
    $('#row8').textbox({value:'18'});
   // $('#row16').textbox({value:'18'});
    $('#row18').textbox({value:'22.75'});
    
   
    $('#allrow').textbox({value:'21'});
    $('#column_a').textbox({value:'14.50'});
    $('#column_b').textbox({value:'6.75'});
    $('#column_c').textbox({value:'8.38'});
    $('#column_d').textbox({value:'5.25'});
    $('#column_e').textbox({value:'4.25'});
    $('#column_f').textbox({value:'6'});
    $('#column_g').textbox({value:'12.88'});
    $('#column_h').textbox({value:'8.38'});
    $('#column_i').textbox({value:'8.38'});
    $('#column_j').textbox({value:'8.38'});
    

}
  </script>

