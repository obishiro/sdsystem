 <div id="menu_formsd44" >
         
        <a href="javascript:void(0)" onclick="saveconfigprintsd44()" class="easyui-linkbutton" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" >บันทึกข้อมูล</a>
        &nbsp;&nbsp;
        <a href="javascript:void(0)" onclick="clearconfigprintsd44()" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',size:'large',iconAlign:'top'">คืนค่าข้อมูลเดิม</a>
   </div>
 
   {!! Form::open(['id'=>'form_saveconfigprintsd44', 'method' => 'post']) !!}
<div class="col-4">
 <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความสูง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 1</label>
               <input class="easyui-textbox" name="row1" id="row1" style="width:95%" data-options="required:true" value="{{ $c->row1}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 10</label>
               <input class="easyui-textbox" name="row10" id="row10" style="width:95%" data-options="required:true" value="{{ $c->row10}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-12">
                <label for="">ความสูงแต่ละบรรทัด</label>
               <input class="easyui-textbox" name="allrow" id="allrow" style="width:95%" data-options="required:true" value="{{ $c->allrow}}">
                    
        </div>
        </div>
    </div>

     <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความกว้าง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ A</label>
               <input class="easyui-textbox" name="column_a" id="column_a" style="width:95%" data-options="required:true" value="{{ $c->column_a}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ B</label>
               <input class="easyui-textbox" name="column_b" id="column_b" style="width:95%" data-options="required:true" value="{{ $c->column_b}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ C</label>
               <input class="easyui-textbox" name="column_c" id="column_c" style="width:95%" data-options="required:true" value="{{ $c->column_c}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ D</label>
               <input class="easyui-textbox" name="column_d" id="column_d" style="width:95%" data-options="required:true" value="{{ $c->column_d}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ E</label>
               <input class="easyui-textbox" name="column_e" id="column_e" style="width:95%" data-options="required:true" value="{{ $c->column_e}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ F</label>
               <input class="easyui-textbox" name="column_f" id="column_f" style="width:95%" data-options="required:true" value="{{ $c->column_f}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ G</label>
               <input class="easyui-textbox" name="column_g" id="column_g" style="width:95%" data-options="required:true" value="{{ $c->column_g}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ H</label>
               <input class="easyui-textbox" name="column_h" id="column_h" style="width:95%" data-options="required:true" value="{{ $c->column_h}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ I</label>
               <input class="easyui-textbox" name="column_i" id="column_i" style="width:95%" data-options="required:true" value="{{ $c->column_i}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ J</label>
               <input class="easyui-textbox" name="column_j" id="column_j" style="width:95%" data-options="required:true" value="{{ $c->column_j}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ K</label>
               <input class="easyui-textbox" name="column_k" id="column_k" style="width:95%" data-options="required:true" value="{{ $c->column_k}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ L</label>
               <input class="easyui-textbox" name="column_l" id="column_l" style="width:95%" data-options="required:true" value="{{ $c->column_l}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ M</label>
               <input class="easyui-textbox" name="column_m" id="column_m" style="width:95%" data-options="required:true" value="{{ $c->column_m}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ N</label>
               <input class="easyui-textbox" name="column_n" id="column_n" style="width:95%" data-options="required:true" value="{{ $c->column_n}}">
        </div>
         
        </div>
    </div>

  </div>
  </form>
  <div class="col-8">
    <div class="easyui-panel" title="ผังประกอบการตั้งค่า" style="width:100%;padding:10px;">
      <img src="{{ URL::to('images/export_template/sd44.png')}}" width="98%" alt="">
    </div>
  </div>
  <script type="text/javascript">
    function saveconfigprintsd44() {
         $('#form_saveconfigprintsd44').form('submit',{
            url: URL+'/configprint/saveconfigprintsd44',
            cache:false,
            data:$('#form_saveconfigprintsd44').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลเรียบร้อยแล้ว!", 'info');
                $('#main_layout').panel({href:URL+'/configprint/sd44'});
                

            } 
          
           
        }
    });
}
function clearconfigprintsd44() {
    $('#row1').textbox({value:'270'});
    $('#row9').textbox({value:'218.25'});
    $('#allrow').textbox({value:'27.75'});
    $('#column_a').textbox({value:'16.22'});
    $('#column_b').textbox({value:'8.67'});
    $('#column_c').textbox({value:'4.78'});
    $('#column_d').textbox({value:'5.56'});
    $('#column_e').textbox({value:'4.11'});
    $('#column_f').textbox({value:'7.44'});
    $('#column_g').textbox({value:'3.89'});
    $('#column_h').textbox({value:'4'});
    $('#column_i').textbox({value:'4'});
    $('#column_j').textbox({value:'3.44'});
    $('#column_k').textbox({value:'3.67'});
    $('#column_l').textbox({value:'3.22'});
    $('#column_m').textbox({value:'4.22'});
    $('#column_n').textbox({value:'8.11'});


}
  </script>

