  <meta name="csrf-token" content="{{ csrf_token() }}">
 <div style="margin:5px;">

    <table id="dg" title="ข้อมูลผู้ใช้งาน"  class="easyui-datagrid" style="width:100%;height:500px"
            url="result/profile"
            toolbar="#toolbar" pagination="true"
            rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
            	<th field="pid" width="30">เลขประจำตัวประชาชน</th>
                <th field="sol_id" width="30">เลขประจำตัวทหาร</th>
                <th field="prefix_name" width="20">คำนำหน้า (ยศ)</th>
                <th field="firstname" width="50">ชื่อตัว</th>
                <th field="lastname" width="50">นามสกุลตัว</th>
                <th field="position_name" width="50">ตำแหน่ง</th>
            </tr>
        </thead>
    </table>
    </div>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="newUser()">เพิ่มข้อมูลใหม่</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">แก้ไขข้อมูล</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">ลบข้อมูล</a>
    </div>
    
    <div id="dlg" class="easyui-dialog" style="width:500px"
            closed="true" buttons="#dlg-buttons" data-options="modal:true">
       {!! Form::open(['id'=>'fm','style'=>'margin:0;padding:20px 50px', 'method' => 'post']) !!}
  
            <div style="margin-bottom:10px">
       
                <input name="pid" id="pid" label="เลขประจำตัวประชาชน" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="sol_id" id="sol_id"  label="เลขประจำตัวทหาร" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="prefix" id="prefix" class="easyui-combobox" required="true" label="คำนำหน้า (ยศ)" style="width:100%">
            </div>
            <div style="margin-bottom:10px">
                <input name="firstname" class="easyui-textbox" required="true"  label="ชื่อตัว" style="width:100%">
            </div>
             <div style="margin-bottom:10px">
                <input name="lastname" class="easyui-textbox" required="true"  label="นามสกุลตัว" style="width:100%">
            </div>
             <div style="margin-bottom:10px">
                <input name="position" id="position"  class="easyui-combobox" required="true" label="ตำแหน่ง" style="width:100%">
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()" >บันทึกข้อมูล</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">ยกเลิก</a>
    </div>
     
    <script type="text/javascript">

        var url;
        function newUser(){
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','เพิ่มข้อมูลผู้ใช้ใหม่');
            $('#fm').form('clear');
            url = URL+'/save/profile';
        }
        function editUser(){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                $('#dlg').dialog('open').dialog('center').dialog('setTitle','แก้ไขข้อมูล');
                $('#fm').form('load',row);
                url = 'edituser/'+row.pid;
            }
        }
        function saveUser(){
            $('#fm').form('submit',{
               
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(result){
                 var result = eval('('+result+')');
                if (result.errorMsg){
                    $.messager.alert({
                        title: 'Error',
                        msg: result.errorMsg
                    });
                }else{
                    if (result.DelAdmin=="1") {
                    $.messager.confirm('เพิ่มข้อมูลผู้ใช้ใหม่', 'ท่านต้องเข้าสู่ระบบด้วย รหัสผู้ใช้งานและรหัสผ่านใหม่ ', function(r){
                            if (r){
                                window.location.href=URL+'/';
                            }
                    });

                    }else{
                    $('#dlg').dialog('close');        // close the dialog
                    $('#dg').datagrid('reload');    // reload the user data
                        }
                }
                    
                }
            });
        }
        function destroyUser(){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                $.messager.confirm('ยืนยัน','ต้องการลบข้อมูลจริงหรือไม่ ?',function(r){
                    if (r){
                        $.post('deleteuser',{id:row.pid},function(result){
                            if (result.success){
                                $('#dg').datagrid('reload');    // reload the user data
                            } else {
                                $.messager.show({    // show error message
                                    title: 'Error',
                                    msg: result.errorMsg
                                });
                            }
                        },'json');
                    }
                });
            }
        }
       $('#pid').textbox({  required:true }).textbox('textbox').mask("9-9999-99999-99-9",{placeholder:" "});
       $('#sol_id').textbox({  required:true }).textbox('textbox').mask("9-99-99-99999",{placeholder:" "});
       $('#position').combobox({
        url:URL+'/data/position',
        valueField:'position_no',
        textField:'position_name'
        });
        $('#prefix').combobox({
        url:URL+'/data/prefix',
        valueField:'prefix_no',
        textField:'prefix_name'
        });
    </script>
{{-- http://www.jeasyui.com/forum/index.php?topic=532.0 --}}