 <div id="menu_formsd1" >
         
        <a href="javascript:void(0)" onclick="saveconfigprintsd1()" class="easyui-linkbutton" data-options="iconCls:'icon-large-save',size:'large',iconAlign:'top'" >บันทึกข้อมูล</a>
        &nbsp;&nbsp;
        <a href="javascript:void(0)" onclick="clearconfigprintsd1()" class="easyui-linkbutton" data-options="iconCls:'icon-refresh',size:'large',iconAlign:'top'">คืนค่าข้อมูลเดิม</a>
   </div>
 
   {!! Form::open(['id'=>'form_saveconfigprintsd1', 'method' => 'post']) !!}
<div class="col-4">
 <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความสูง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 1</label>
               <input class="easyui-textbox" name="row1" id="row1" style="width:95%" data-options="required:true" value="{{ $c->row1}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 11</label>
               <input class="easyui-textbox" name="row11" id="row11" style="width:95%" data-options="required:true" value="{{ $c->row11}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 13</label>
               <input class="easyui-textbox" name="row13" id="row13" style="width:95%" data-options="required:true" value="{{ $c->row13}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 14</label>
               <input class="easyui-textbox" name="row14" id="row14" style="width:95%" data-options="required:true" value="{{ $c->row14}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 16</label>
               <input class="easyui-textbox" name="row16" id="row16" style="width:95%" data-options="required:true" value="{{ $c->row16}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 17</label>
               <input class="easyui-textbox" name="row17" id="row17" style="width:95%" data-options="required:true" value="{{ $c->row17}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 18</label>
               <input class="easyui-textbox" name="row18" id="row18" style="width:95%" data-options="required:true" value="{{ $c->row18}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 19</label>
               <input class="easyui-textbox" name="row19" id="row19" style="width:95%" data-options="required:true" value="{{ $c->row19}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 22</label>
               <input class="easyui-textbox" name="row22" id="row22" style="width:95%" data-options="required:true" value="{{ $c->row22}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 27</label>
               <input class="easyui-textbox" name="row27" id="row27" style="width:95%" data-options="required:true" value="{{ $c->row27}}">
                    
        </div>
        <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 29</label>
               <input class="easyui-textbox" name="row29" id="row29" style="width:95%" data-options="required:true" value="{{ $c->row29}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 30</label>
               <input class="easyui-textbox" name="row30" id="row30" style="width:95%" data-options="required:true" value="{{ $c->row30}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแถวที่ 32</label>
               <input class="easyui-textbox" name="row32" id="row32" style="width:95%" data-options="required:true" value="{{ $c->row32}}">
                    
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">ความสูงแต่ละบรรทัด</label>
               <input class="easyui-textbox" name="allrow" id="allrow" style="width:95%" data-options="required:true" value="{{ $c->allrow}}">
                    
        </div>
        </div>
    </div>

     <div class="col-12">
        <div class="easyui-panel" title="ตั้งค่าระดับความกว้าง (Points)" style="width:100%;padding:10px;">
        <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ A</label>
               <input class="easyui-textbox" name="column_a" id="column_a" style="width:95%" data-options="required:true" value="{{ $c->column_a}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ B</label>
               <input class="easyui-textbox" name="column_b" id="column_b" style="width:95%" data-options="required:true" value="{{ $c->column_b}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ C</label>
               <input class="easyui-textbox" name="column_c" id="column_c" style="width:95%" data-options="required:true" value="{{ $c->column_c}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ D</label>
               <input class="easyui-textbox" name="column_d" id="column_d" style="width:95%" data-options="required:true" value="{{ $c->column_d}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ E</label>
               <input class="easyui-textbox" name="column_e" id="column_e" style="width:95%" data-options="required:true" value="{{ $c->column_e}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ F</label>
               <input class="easyui-textbox" name="column_f" id="column_f" style="width:95%" data-options="required:true" value="{{ $c->column_f}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ G</label>
               <input class="easyui-textbox" name="column_g" id="column_g" style="width:95%" data-options="required:true" value="{{ $c->column_g}}">            
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ H</label>
               <input class="easyui-textbox" name="column_h" id="column_h" style="width:95%" data-options="required:true" value="{{ $c->column_h}}">
        </div>
         <div style="margin-bottom:2px" class="col-5">
                <label for="">คอลัมน์ I</label>
               <input class="easyui-textbox" name="column_i" id="column_i" style="width:95%" data-options="required:true" value="{{ $c->column_i}}">            
        </div>
         
         
        </div>
    </div>

  </div>
  </form>
  <div class="col-8">
    <div class="easyui-panel" title="ผังประกอบการตั้งค่า" style="width:100%;padding:10px;">
      <img src="{{ URL::to('images/export_template/sd1.png')}}" width="98%" alt="">
    </div>
  </div>
  <script type="text/javascript">
    function saveconfigprintsd1() {
         $('#form_saveconfigprintsd1').form('submit',{
            url: URL+'/configprint/saveconfigprintsd1',
            cache:false,
            data:$('#form_saveconfigprintsd1').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลเรียบร้อยแล้ว!", 'info');
                $('#main_layout').panel({href:URL+'/configprint/sd1'});
                

            } 
          
           
        }
    });
}
function clearconfigprintsd1() {
    $('#row1').textbox({value:'35.25'});
    $('#row11').textbox({value:'15'});
    $('#row13').textbox({value:'15'});
    $('#row14').textbox({value:'27.75'});
    $('#row16').textbox({value:'27'});
    $('#row17').textbox({value:'25.50'});
    $('#row18').textbox({value:'27'});
    $('#row19').textbox({value:'26.25'});
    $('#row22').textbox({value:'32.25'});
    $('#row27').textbox({value:'28.50'});
    $('#row29').textbox({value:'29.25'});
    $('#row30').textbox({value:'21.75'});
    $('#row32').textbox({value:'24.75'});
   
    $('#allrow').textbox({value:'24'});
    $('#column_a').textbox({value:'28.75'});
    $('#column_b').textbox({value:'6.75'});
    $('#column_c').textbox({value:'6.88'});
    $('#column_d').textbox({value:'6.75'});
    $('#column_e').textbox({value:'3'});
    $('#column_f').textbox({value:'10.63'});
    $('#column_g').textbox({value:'10.25'});
    $('#column_h').textbox({value:'4.75'});
    $('#column_i').textbox({value:'6'});
    

}
  </script>

