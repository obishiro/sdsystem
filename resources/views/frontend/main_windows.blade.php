@extends('master')
@section('title','ระบบสารสนเทศสายงานสัสดี')
@section('content')
@include('frontend.menu_top')
   <div class="easyui-layout" id="mainwindows" style="min-height:630px;height:100%;" >

       {{--  <div id="p" data-options="region:'west'" title="เมนูระบบต่างๆ" style="width:20%;">
           @include('frontend.menu')
        </div> --}}
        <div data-options="region:'center',href:''" id="main_layout" title="">
        
        </div>
       {{--  <div data-options="region:'south'"  class="footer">
        	ระบบสารสนเทศสายงานสัสดี เวอร์ชั่น 0.1
        	<span>เข้าใช้ระบบโดย : {{$u->prefix_name}} {{$u->firstname}} {{ $u->lastname }}</span>
        	<span>ตำแหน่ง : {{$u->position_name}}</span>
        	<span>วันที่เข้าใช้ระบบ  {{ Helpers::DateEN(date('Y-m-d'))}}</span>
        </div> --}}
    </div>
   
	
@stop
@section('script')
   <script src="{{ URL::to('js/main_windows.js?='.str_random(8).'')}}"></script>
   <script src="{{ URL::to('js/form.js?='.str_random(8).'')}}"></script>

@stop
