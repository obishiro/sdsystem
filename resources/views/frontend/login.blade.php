@extends('master')
@section('title','ระบบสารสนเทศสายงานสัสดี')
@section('content')
 
<div id="win">
    <img src="{{ URL::to('images/logo.jpg')}}" alt="" class="logo_login">
        <form id="form_login" method="post" style="margin-left:15px">
    
            {!! csrf_field() !!}
            <div style="margin-bottom:10px;margin-top:10px">
                <input  name="txt_name" id="txt_name"  >
            </div>
            <div style="margin-bottom:10px">
                <input class="easyui-passwordbox"  name="txt_password" id="txt_password" >
            </div>
   
        </form>
        <div style="text-align:center;padding:5px 0">
            <a href="javascript:void(0)" data-options="iconCls:'icon-ok'"  class="easyui-linkbutton" onclick="submitForm()" style="width:100px">เข้าสู่ระบบ</a>
            <a href="javascript:void(0)" style="width:80px"  data-options="iconCls:'icon-remove'"  class="easyui-linkbutton" onclick="clearForm()">ยกเลิก</a>
        </div>
</div>
<div id="footer" style="padding:5px;">ระบบสารสนเทศสายงานสัสดี เวอร์ชั่น ๐.๑</div>
@stop
@section('script')
   <script src="{{ URL::to('js/login.js?='.str_random(8).'')}}"></script>
   <script type="text/javascript">
   $('#txt_name').textbox('textbox').mask("9-9999-99999-99-9",{placeholder:" "});
   $('#txt_name').focus();
   </script>

@stop