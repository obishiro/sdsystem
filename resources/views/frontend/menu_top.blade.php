 
  
    <div class="easyui-panel" style="padding:5px;">
        <a href="#" class="easyui-menubutton" data-options="menu:'#mm1',iconCls:'icon-config' ">ตั้งค่าระบบ</a>
    
        <a href="#" class="easyui-menubutton" data-options="menu:'#mm2',iconCls:'icon-folder'">บัญชีทหารกองเกิน</a>
        <a href="#" class="easyui-menubutton" data-options="menu:'#mm3',iconCls:'icon-soldier'">การเรียกคนเข้ากองประจำการ</a>
        <a href="javascript:void(0)" onclick="exit()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-exit'">ออกจากระบบ</a>
    </div>
    <div style="padding:5px 8px;">
       <a href="javascript:void(0)" onclick="datathaiperson()" class="easyui-linkbutton" data-options="iconCls:'users-icon-large',size:'large',iconAlign:'top'">บัญชีข้อมูลชายไทย</a>

        <a href="javascript:void(0)" onclick="formsd44()" class="easyui-linkbutton" data-options="iconCls:'icon-large-user',size:'large',iconAlign:'top'">แสดงตนลงบัญชี</a>
        {{--  <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-large-shapes',size:'large',iconAlign:'top'">Shapes</a>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-large-smartart',size:'large',iconAlign:'top'">SmartArt</a>
        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-large-chart',size:'large',iconAlign:'top'">Chart</a> --}}
    </div>
    <div id="mm1">
           
        <div  data-options="iconCls:'icon-config'">
            <span >ตั้งค่าข้อมูลพื้นฐาน</span>
            <div>
                <div>
                    <a href="javascript:void(0)" onclick="configsystem()">ตั้งค่าข้อมูลพื้นฐาน
                    </a>
                </div>
                <div><a href="javascript:void(0)" onclick="configmooban()">ตั้งค่าข้อมูลหมู่บ้าน</a></div>
                <div>
                    <a href="javascript:void(0)" onclick="configuser()">ข้อมูลผู้ใช้งาน</a>
                </div>
                
            </div>
        </div>
         <div  data-options="iconCls:'icon-config'">
            <span >ตั้งค่าขนาดแบบพิมพ์</span>
            <div>
                <div data-options="iconCls:'icon-print'"><a href="javascript:void(0)" onclick="configprintsd44()">แบบพิมพ์แสดงตนฯ (สด.44)</a>
                </div>
                <div data-options="iconCls:'icon-print'"><a href="javascript:void(0)" onclick="configprintsd1()">แบบพิมพ์บัญชีทหารกองเกิน (สด.1)</a>
                </div>
                <div data-options="iconCls:'icon-print'"><a href="javascript:void(0)" onclick="configprintsd1()">แบบพิมพ์ (สด.2)</a>
                </div>
                <div data-options="iconCls:'icon-print'"><a href="javascript:void(0)" onclick="configprintsd9()">แบบพิมพ์ใบสำคัญ สด.9</a>
                </div>
                <div data-options="iconCls:'icon-print'"><a href="javascript:void(0)" onclick="configprintsd35()">แบบพิมพ์หมายเรียกฯ (สด.35)</a>
                </div>
                
            </div>
        </div>
        <div data-options="iconCls:'icon-exit'"><a href="javascript:void(0)" onclick="exit()">ออกจากระบบ</a></div>
    
    </div>
    
    <div id="mm2">
         <div  data-options="iconCls:'icon-users'">
            <span>บัญชีข้อมูลชายไทย</span>

                <div>
                    <div><a href="javascript:void(0)" onclick="formthaiperson()">นำเข้าข้อมูลชายไทย</a></div>

                    <div><a href="javascript:void(0)" onclick="datathaiperson()">บัญชีข้อมูลชายไทย</a></div>
                </div>

        </div>
           
         
        <div data-options="iconCls:'icon-folder'">
            <span >ระบบบัญชีทหารกองเกิน</span>
            <div>
                <div><a href="javascript:void()" onclick="formsd44()"><span>แสดงตนลงบัญชี</span></a></div>
                 <div><a href="javascript:void(0)" onclick="formsd1()"><span>บัญชีทหารกองเกิน (แบบ สด.1)</span></a></div>
                   
                    <div><a href="javascript:void(0)" onclick="formsd1_m16()">
                    จำนวนผู้ลงบัญชีตาม ม.16</a>
                    </div>
                    <div><a href="javascript:void(0)" onclick="formsd1_m18()">จำนวนผู้ลงบัญชีตาม ม.18</a>
                        </div>
                    <div>
                    <a href="javascript:void(0)" onclick="formsortsd1()">จัดเรียงเลข สด.1</a>
                    
                        </div>
                     <div>สรุปข้อมูลการลงบัญชีประจำเดือน
                         </div>
            </div>
               
        </div>
        <div data-options="iconCls:'icon-horn' ">
            <span>ระบบจัดทำประกาศ</span>
            <div>
                <div>
                    <span>
                        <a href="javascript:void(0)" onclick="">จัดทำประกาศ (แบบ สด.38)</a>
                    </span>
                </div>
                <div>
                    <span>
                        <a href="javascript:void(0)" onclick="">จัดทำประกาศ (แบบ สด.39)</a>
                    </span>
                </div>
                </div>
        </div>
    </div>
    <div id="mm3">
        <div data-options="iconCls:'icon-star'">
            <span>ระบบจัดทำหมายเรียก</span>
                <div>
                    <div>
                        <a href="javascript:void(0)" onclick="formsd35year()">จัดทำหมายเรียกประจำปี</a>
                    </div>
                      <div>
                        <a href="javascript:void(0)" onclick="">จัดทำหมายเรียกคนผ่อนผัน</a>
                    </div>
                    <div>
                        <a href="javascript:void(0)" onclick="">รายงานวันรับหมายเรียก</a>
                    </div>
                </div>
        </div>
         <div  data-options="iconCls:'icon-soldier'">
            <span >ระบบบัญชีเรียกทหารกองเกิน</span>
            <div>
                <div><a href="javascript:void(0)" onclick="">บัญชีคนหลีกเลี่ยงขัดขืน</a></div>
                <div><a href="javascript:void(0)" onclick="">บัญชีคนที่ขาดการตรวจเลือก</a></div>
                <div><a href="javascript:void(0)" onclick="">บัญชีคนที่อยู่ในกำหนดเรียกธรรมดา</a></div>
                <div><a href="javascript:void(0)" onclick="">บัญชีคนที่พ้นจากฐานะยกเว้นผ่อนผัน</a></div>
                <div><a href="javascript:void(0)" onclick="">บัญชีคนผ่อนผัน</a></div>
                
            </div>
        </div>
        <div>
            <span>ระบบหลังการตรวจเลือก</span>
            <div>
                <div>
                    <a href="javascript:void(0)">รายงานผลการตรวจเลือก</a>
                </div>
                 <div>
                    <a href="javascript:void(0)">บัญชี ก.</a>
                </div>
                 <div>
                    <a href="javascript:void(0)">บัญชีคนที่ส่งเข้ากองประจำการ (แบบ สด.18)</a>
                </div>
                <div>
                    <a href="javascript:void(0)">กง.2.1</a>
                </div>
            </div>
        </div>
    </div>