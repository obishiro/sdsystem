        <ul id="tt" class="easyui-tree"  data-options="animate:true,lines:true">
        <li>

            <span>ระบบลงทะเบียนบัญชีทหารกองเกิน</span>
            <ul>
            
             <li><span><a href="javascript:void(0)" onclick="formsd44()">แสดงตนเพื่อลงบัญชีฯ (แบบ สด.๔๔)</a></span></li>
               <li data-options="state:'closed'">
                <span>ระบบข้อมูลชายไทย</span>
                <ul>
                    <li><span><a href="javascript:void(0)" onclick="">บันทึกข้อมูลชายไทย</a></span></li>
                    <li><span><a href="javascript:void(0)" onclick="">ข้อมูลชายไทย</a></span></li>

                </ul>
                 </li>

             <li data-options="state:'closed'">
                <span>ระบบบัญชีทหารกองเกิน</span>
                <ul>
                    <li><span><a href="javascript:void(0)" onclick="formsd1()">บัญชีทหารกองเกิน (แบบ สด.๑)</a></span></li>
                   
                    <li><span><a href="javascript:void(0)" onclick="formsd1_m16()">
                    จำนวนผู้ลงบัญชีตาม ม.๑๖</a></span></li>
                    <li><span><a href="javascript:void(0)" onclick="formsd1_m18()">จำนวนผู้ลงบัญชีตาม ม.๑๘</a></span></li>
                    <li><span>
                    <a href="javascript:void(0)" onclick="formsortsd1()">จัดเรียงเลข สด.๑</a>
                    </span></li>
                     <li><span>สรุปข้อมูลการลงบัญชีประจำเดือน</span></li>

                </ul>
            </li>
    {{--          <li data-options="state:'closed'">
                <span>ระบบจัดทำบัญชีทหารกองเกิน</span>
                <ul>
                    <li><span>(แบบ สด.๒)</span></li>
                 
                    <li><span>บัญชีสรุปปกใน</span></li>
                </ul>
            </li> --}}
           <li data-options="state:'closed'">
                <span>ระบบจัดทำประกาศ</span>
                <ul>
                    <li><span>แบบพิมพ์ (แบบ สด.๓๘)</span></li>
                    <li><span>แบบพิมพ์ (แบบ สด.๓๙)</span></li>
                </ul>
            </li>
            <li data-options="state:'closed'">
                <span>ระบบจัดทำหมายเรียก (แบบ สด.๓๕)</span>
                <ul>
                    <li><span><a href="javascript:void(0)" onclick="formsd35year()">จัดทำหมายเรียกประจำปี</a></span></li>
                    <li><span>ข้อมูลการรับหมายเรียก</span></li>
                </ul>
            </li>
      </ul>
        </li>
        <li data-options="state:'closed'">
            <span>
                ระบบการตรวจเลือกทหารกองเกินฯ
            </span>
                <ul>
                    <li>
                        <span><a href="javascript:void(0)"" onclick="">บัญชีคนหลีกเลียงขัดขืน</a></span>
                    </li>
                    <li>
                        <span><a href="javascript:void(0)"" onclick="">บัญชีคนที่ขาดการตรวจเลือก</a></span>
                    </li>
                     <li>
                        <span><a href="javascript:void(0)"" onclick="">บัญชีคนที่อยู่ในกำหนดเรียกธรรมดา</a></span>
                    </li>
                     <li>
                        <span><a href="javascript:void(0)"" onclick="">บัญชีคนที่พ้นจากฐานะยกเว้นผ่อนผัน</a></span>
                    </li>
                     <li>
                        <span><a href="javascript:void(0)"" onclick="">บัญชีคนผ่อนผัน</a></span>
                    </li>
                </ul>
        </li>
        <li  data-options="state:'closed'">
            <span>ตั้งค่าระบบ</span>
            <ul>
                <li><a href="javascript:void(0)"><span onclick="configsystem()">ตั้งค่าข้อมูลพื้นฐาน</span></a></li>
                <li><a href="javascript:void(0)"><span onclick="configuser()">ข้อมูลผู้ใช้งาน</span></a></li>
            </ul>
        </li>
         <li  data-options="state:'closed'">
            <span>ตั้งค่าขนาดแบบพิมพ์</span>
            <ul>
                <li><a href="javascript:void(0)"><span onclick="configprintsd44()">แบบพิมพ์แสดงตนฯ (สด.๔๔)</span></a></li>
                <li><a href="javascript:void(0)"><span onclick="configprintsd1()">แบบพิมพ์บัญชีทหารกองเกิน (สด.๑)</span></a></li>
                <li><a href="javascript:void(0)"><span onclick="configprintsd1()">แบบพิมพ์ (สด.๒)</span></a></li>
                <li><a href="javascript:void(0)"><span onclick="configprintsd9()">แบบพิมพ์ใบสำคัญ สด.๙</span></a></li>
                <li><a href="javascript:void(0)"><span onclick="configprintsd35()">แบบพิมพ์หมายเรียกฯ (สด.๓๕)</span></a></li>
            </ul>
        </li>
         <li data-options="iconCls:'icon-help'" >
            <a href="javascript:void(0)">
            <span onclick="help()">ช่วยเหลือ</span>
            </a>
        </li>
        <li data-options="iconCls:'icon-new-exit'" >
            <a href="javascript:void(0)">
            <span onclick="exit()">ออกจากระบบ</span>
            </a>
        </li>
    </ul>
    