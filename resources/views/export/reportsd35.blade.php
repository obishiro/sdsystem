<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <style>
	.red {
	 color: #ff0000;
	}
	.center {
		text-align: center;
	}
	.right {
		text-align: right;
	}
</style>
<table>
  	<tr>
  		<td colspan="10" align="center" style="font-weight: bold">หมายเรียกเข้ารับราชการทหาร (แบบ สด.๓๕)</td>
  	</tr>
  	<tr>
  		<td  colspan="10" align="center" style="font-weight: bold">ตำบล {{ $tumbon }}</td>
  	</tr>
	 <tr>
	 	<td style="border:1px solid #000000" align="center" valign="middle" rowspan="2">เลขที่</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" rowspan="2" colspan="2">ชื่อ - สกุล</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" colspan="2">ชื่อ</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" colspan="2">ภูมิลำเนาทหาร</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle">วันที่ </td>
	 	<td style="border:1px solid #000000; width:9.5pt" align="center"    rowspan="2">หมายเหตุ</td>
	 	<td style="border:1px solid #000000; width:8.5pt" align="center" valign="middle" rowspan="2">เลขสด.๑๖ </td>
	 	 
	 </tr>
	 	 <tr>
	 	 <td style="border:1px solid #000000" align="center" valign="middle"></td>
	 	 <td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle"></td>
	 	 <td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle"></td>
	 	<td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle">บิดา</td>
	 	<td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle">มารดา</td>
	 	<td style="border:1px solid #000000; width:6.5pt" align="center" valign="middle">เลขที่</td>
	 	<td style="border:1px solid #000000; width:6.5pt" align="center" valign="middle">หมู่ที่</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle">รับหมายฯ</td>
	  
	 	 
	 </tr>
 	 <?php $i=1;?>
	 @foreach($sql as $data =>$s)
	<tr>
		<td style="border:1px solid #000000;width:8.5pt" class="right">{{ Helpers::cthai($i)}}/{{ $year_g}}</td>
		<td style="border:1px solid #000000">{{ $s->firstname}}</td>
		<td style="border:1px solid #000000">{{ $s->lastname}}</td>
		<td style="border:1px solid #000000">{{ $s->fa_name}}</td>
		<td style="border:1px solid #000000">{{ $s->ma_name}}</td>
		<td style="border:1px solid #000000" align="center">{{ Helpers::cthai($s->home_id)}}</td>
		<td style="border:1px solid #000000" align="center">{{ Helpers::cthai($s->moo)}}</td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
	</tr>
	<?php $i++;?>	 
 @endforeach 
  	<tr>
  		<td colspan="10" align="center" style="font-weight: bold">หมายเรียกเข้ารับราชการทหาร (แบบ สด.๓๕) สำหรับคนอายุอื่น</td>
  	</tr>
  	<tr>
	 	<td style="border:1px solid #000000" align="center" valign="middle" rowspan="2">เลขที่</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" rowspan="2" colspan="2">ชื่อ - สกุล</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" colspan="2">ชื่อ</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle" colspan="2">ภูมิลำเนาทหาร</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle">วันที่ </td>
	 	<td style="border:1px solid #000000; width:9.5pt" align="center"    rowspan="2">หมายเหตุ</td>
	 	<td style="border:1px solid #000000; width:8.5pt" align="center" valign="middle" rowspan="2">เลขสด.๑๖ </td>
	 	 
	 </tr>
	 	 <tr>
	 	 <td style="border:1px solid #000000" align="center" valign="middle"></td>
	 	 <td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle"></td>
	 	 <td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle"></td>
	 	<td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle">บิดา</td>
	 	<td style="border:1px solid #000000; width:10.5pt" align="center" valign="middle">มารดา</td>
	 	<td style="border:1px solid #000000; width:6.5pt" align="center" valign="middle">เลขที่</td>
	 	<td style="border:1px solid #000000; width:6.5pt" align="center" valign="middle">หมู่ที่</td>
	 	<td style="border:1px solid #000000" align="center" valign="middle">รับหมายฯ</td>
	  
	 	 
	 </tr>
  	<?php for($ii=1;$ii<=10;$ii++) { ?>
  	 <tr>
		<td style="border:1px solid #000000" class="right">{{ Helpers::cthai($ii)}}/{{ $year_g}}</td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000" align="center"></td>
		<td style="border:1px solid #000000" align="center"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
		<td style="border:1px solid #000000"></td>
	</tr>
	<?php } ?>
	<tr>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>ตรวจถูกต้อง</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>

	</tr>
</table>
	