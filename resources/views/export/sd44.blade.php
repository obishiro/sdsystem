<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
	<tr> 
	{{-- Row1 --}}
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
	{{-- Row2 --}}
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>ที่ว่าการอำเภอ{{$s->AMPHUR_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
	{{-- Row3 --}}
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>&nbsp;{{ Helpers::Datewrite($s->date_regis) }}</td>
		<td></td>
		<td></td>
		<td>{{ Helpers::Monthwrite($s->date_regis)}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::Yearwrite($s->date_regis)}}</td>
	</tr>

	<tr>
		<td></td>
		<td></td>
		<td>นาย{{ $s->firstname}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->lastname}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::Yearwrite($s->dob)}}</td>
	</tr>
		<tr>
		<td></td>
		<td>ไทย</td>
		<td></td>
		<td>{{ $s->fa_name}} &nbsp; {{ $s->fa_lname}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->ma_name}} &nbsp; {{ $s->ma_lname}}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::cthai($s->home_id)}}</td>
		<td></td>
		<td></td>
		<td>{{ $s->road}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->trok}}</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{{ $s->soy}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::cthai($s->moo) }}</td>
		<td></td>
		<td></td>
		<td>{{ $s->DISTRICT_NAME }}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{{ $s->AMPHUR_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->PROVINCE_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->AMPHUR_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td> &nbsp;&nbsp;{{ Helpers::cthai($s->pcard_id)}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->pcard_by}}</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{{ Helpers::Datewrite($s->pcard_issue) }}</td>
		<td></td>
		<td>{{ Helpers::Monthwrite($s->pcard_issue) }}</td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::Yearwrite($s->pcard_issue) }}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::cthai($s->home_id)}}</td>
		<td></td>
		<td></td>
		<td>{{ $s->road}}</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{{ $s->trok}}</td>
		<td></td>
		<td></td>
		<td>{{ $s->soy}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ Helpers::cthai($s->moo)}}</td>
		<td></td>
		<td></td>
		<td>{{ $s->DISTRICT_NAME }}</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td>{{ $s->AMPHUR_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td>{{ $s->PROVINCE_NAME}}</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>