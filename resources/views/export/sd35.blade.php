<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.red {
	 color: #ff0000;
	}
	.center {
		text-align: center;
	}
	.right {
		text-align: right;
	}
</style>

<table>
	 <?php $i =1;?>
	 @foreach($sql as $data =>$s)
	
	 
	 <tr> {{-- A --}}
	 	<td>เลขประจำตัวประชาชน</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td class="red">ในวันตรวจเลือกนำหลักฐานมา ดังนี้</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td>{{ Helpers::FnID($s->pid)}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td class="red">๑.หมายเรียกฯ (แบบ สด.๓๕)</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td class="red">๒.ใบสำคัญทหารกองเกิน (แบบ สด.๙)</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td class="red">๓.บัตรประจำตัวประชาชน</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td class="red">๔.วุฒิการศึกษา ตั้งแต่ ม.๖ ขึ้นไป</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td>{{ Helpers::cthai($i)}}</td>
	 	<td>&nbsp;&nbsp;&nbsp;{{ $year_g}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td>&nbsp;&nbsp;&nbsp;&nbsp;{{ $s->AMPHUR_NAME}}</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td class="center">๑</td>
	 	<td></td>
	 	<td>มกราคม</td>
	 	<td></td>
	 	<td></td>
	 	<td  class="center">{{ $year_g}}</td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td>นาย{{$s->firstname }}</td>
	 	<td></td>
	 	<td></td>
	 	<td>{{ $s->lastname}}</td>
	 	<td></td>
	 	<td  class="right">{{ $age }}</td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td>{{ $s->fa_name}}-{{ $s->ma_name}}</td>
	 	<td></td>
	 	<td></td>
	 	<td>-</td>
	 	<td></td>
	 	<td  class="right">{{ Helpers::cthai($s->home_id) }}</td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td  style="height:19.5pt"></td>
	 	<td  class="center">{{ Helpers::cthai($s->moo) }}</td>
	 	<td></td>
	 	<td>{{ $s->DISTRICT_NAME}}</td>
	 	<td></td>
	 	<td>{{ $s->AMPHUR_NAME}}</td>
	 	<td></td>
	 	<td>{{ $s->PROVINCE_NAME}}</td>
	 	<td></td>
	 </tr>
	 <tr >
	 	<td style="height:21.75pt"></td>
	 	<td></td>
	 	<td></td>
	 	<td>{{ $place }}</td>
	 	<td></td>
	 	<td class="red center">{{ $day }}</td>
	 	<td class="red right">{{ $month }}</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td  style="height:17.25pt"></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td class="red">{{ $year_k}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td  class="center red">๐๗.๐๐</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>   
	 	<td>เลขประจำตัวประชาชน {{ Helpers::FnID($s->pid)}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td style="height:36pt"  class="right">{{ Helpers::cthai($i)}}/{{ $year_g}}</td>
	 	<td></td>
	 	<td></td>
	 	<td>นาย{{ $s->firstname}}</td>
	 	<td></td>
	 	<td></td>
	 	<td>{{ $s->lastname}}</td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	  <tr>
	 	<td></td>
	 	<td>{{$s->fa_name}} - {{ $s->ma_name}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <tr>
	 	<td></td>
	 	<td>{{ Helpers::cthai($s->home_id)}} ม.{{ Helpers::cthai($s->moo)}} ต.{{ $s->DISTRICT_NAME}}</td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
 
 	 <tr>
	 	<td></td>
	 	<td> </td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 	<td></td>
	 </tr>
	 <?php $i++;?>
 @endforeach 

</table>
	