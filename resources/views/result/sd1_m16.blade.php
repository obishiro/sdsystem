  <meta name="csrf-token" content="{{ csrf_token() }}">
 <div style="margin:5px;">
 
 <form id="form_sd2" method="post">
    {!! csrf_field() !!}
 <select class="easyui-combobox col-2"  name="year_dob" id="year_dob">
 {{ Helpers::GetYear()}}
</select> 
  <select class="col-2" name="district" id="district">
       {{ Helpers::GetDistrict()}}
  </select>
    &nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search"  onclick="LoadData()">เรียกดูข้อมูล (แบบ สด.1)</a>
     &nbsp;&nbsp;<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print"  onclick="Print()">ส่งออกข้อมูลพิมพ์ (แบบ สด.2) ประจำปี</a>
  </form>
 
 <div style="height: 5px"></div>
    <table id="dg" title="จำนวนผู้ลงบัญชีตาม ม.๑๖"  class="easyui-datagrid" style="width:100%;height:520px"
            
            toolbar="#toolbar" pagination="true"
            rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
                <th width="8%" field="date_regis">วันลงบัญชี</th>
                <th width="4%" field="sd1_no">สด.1</th>
            	<th width="4%" field="sd9_no">สด.9</th>
            	<th width="13%" field="pid">เลข ปชช.</th>
               
                <th width="8%" field="firstname">ชื่อ</th>
                <th width="8%" field="lastname">นามสกุล</th>
                <th width="7%" field="dob">วดป.เกิด</th>
                <th width="3%" field="age">อายุ</th>
                <th width="5%" field="home_id">เลขที่</th>
                <th width="4%" field="moo">หมู่ที่</th>
                <th width="8%" field="DISTRICT_NAME">ตำบล</th>
                <th width="8%" field="fa_name">บิดา</th>
                <th width="8%" field="fa_lname">นามสกุล</th>
                <th width="8%" field="ma_name">มารดา</th>
                <th width="8%" field="ma_lname">นามสกุล</th>
                <th width="5%" field="section">มาตรา</th>
            </tr>
        </thead>
    </table>
    </div>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editSoldier()">แก้ไขข้อมูล</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="divideSoldier()">จำหน่าย</a>
    </div>
    
 
  
    <script type="text/javascript">

    //  $('#year_dob').combobox({
    //     url:URL+'/data/year',
    //     valueField:'id',
    //     textField:'text'
    // });
     $('#district').combobox({
        url:URL+'/data/district',
        valueField:'DISTRICT_CODE',
        textField:'DISTRICT_NAME'
});
     $('#dg').datagrid({
      striped:true,
      url:"result/sd1_m16"
     });
     function LoadData(){
     	var district = $('#district').combobox('getValue');
     	var year = $('#year_dob').combobox('getValue');
      var y = year - 543;
      if (district=="- เลือกตำบล -")
        {
          district = '0';
        }
     	$('#dg').datagrid({
     	striped:true,
     	url:"loaddatasd1_m16"+'/'+y+'/'+district
     });
     }
     function Print() {
 
  var year_dob = $('#year_dob').combobox('getValue');
  var district = $('#district').combobox('getValue');
  if (year_dob=="- เลือกปีเกิด/ชั้นปี -" || district=="- เลือกตำบล -")
  {
    $.messager.alert('ผลการทำงาน', "กรุณาเลือกปีเกิด/ตำบลด้วย", 'error');
  }else{
 
                  $("#form_sd2").attr("action", URL+"/export/sd2" );
                 $("#form_sd2").submit();
          
          }
   
}
    
    </script>
{{-- http://www.jeasyui.com/forum/index.php?topic=532.0 --}}