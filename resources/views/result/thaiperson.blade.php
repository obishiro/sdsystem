  <meta name="csrf-token" content="{{ csrf_token() }}">

<div style="margin:5px;">
 
  <form  megtod="POST">

 <select class="easyui-combobox col-3"  name="year_dob" id="year_dob">
 {{ Helpers::GetYear()}}
</select> 
  <select class="col-2" name="district" id="district">
       {{ Helpers::GetDistrict()}}
  </select>&nbsp;&nbsp;
    <select class="easyui-combobox col-2" name="status" id="status">
       <option value="9">- สถานะ -</option>
       <option value="0">ยังไม่ลงบัญชี</option>
       <option value="1">ลงบัญชีทหารแล้ว</option>
  </select>&nbsp;&nbsp;

  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search"   onclick="LoadDataThaiPerson()">เรียกดูข้อมูลชายไทย</a>
  </form>
 
 <div style="height: 5px"></div>

    <table id="dg" title="บัญชีข้อมูลชายไทย"  class="easyui-datagrid" style="width:100%;height:500px"
            toolbar="#toolbar" pagination="true"
            rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
                <th  field="pid" width="12%">รหัส ปปช.</th>
            	<th field="firstname" width="12%">ชื่อตัว</th>
                <th field="lastname" width="15%">ชื่อสกุล</th>
                <th field="dob" width="10%">วดป.เกิด</th>
                <th field="faname" width="10%">ชื่อบิดา</th>
                <th field="maname" width="10%">ชื่อมารดา</th>
                <th field="addr" width="7%">บ้านเลขที่</th>
                <th field="moo" width="5%">หมู่ที่</th>
                <th field="tumbon" width="10%">ตำบล</th>
                <th field="links" width="10%" data-options="formatter:formatStatus">แสดงตน</th>
               
            </tr>
        </thead>
    </table>

    </div>
    <div id="toolbar">

        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="">เพิ่มข้อมูลใหม่</a>
        
    <div style="float: right">
    
     
   
     <input class="easyui-searchbox" id="txt_val" data-options="prompt:'กรอกข้อความที่ต้องการค้นหา!',searcher:doSearch" style="width:100%">
    </div>
    </div>
    
  
  
            
     
     
    <script type="text/javascript">
 

    //  $('#year_dob').combobox({
    //     url:URL+'/data/year',
    //     valueField:'id',
    //     textField:'text'
    // });
     $('#district').combobox();
     $('#dg').datagrid({
        striped:true,
     // url:"result/sd1"
     });
     function LoadDataThaiPerson(){
        var district = $('#district').combobox('getValue');
        var year = $('#year_dob').combobox('getValue');
        var status = $('#status').combobox('getValue');
        $('#dg').datagrid({
        striped:true,
        url:"datathaiperson"+'/'+year+'/'+district+'/'+status
     });
     }
     function doSearch(){
      var txt = $('#txt_val').val();
      $('#dg').datagrid({
        striped:true,
        url:"search/thaiperson/"+txt
      });
     }
    function formatStatus(val,row) {
      var person_id = row.pid;
      var newPID= person_id.split(' ').join('');
       if(val==0)
       {
        return '<a href="javascript:void(0)" onclick="formloaddatathaiperson(\''+newPID+'\')" style="color:#ffffff" class=\'easyui-linkbutton c5\' data-options="iconCls:\'icon-remove\'">ยังไม่ลงบัญชี</a>';
       }else{
        return '<span style="color:green;">ลงบัญชีแล้ว</span>';

       }
    }
    $(function(){
    $('#dg').datagrid({
        onLoadSuccess:function(){
            $(this).datagrid('getPanel').find('.easyui-linkbutton').each(function(){
                $(this).linkbutton({
                    // onClick:function(){
                    //     var id = $(this).attr('row-id');
                    //     var act = $(this).attr('act');
                    //     console.log(act)
                    //     // destroy the row
                    // }
                })
            })
        }
    })
})    
   function formloaddatathaiperson(pid) {
   //   alert(pid);
   //$('#form_sd44').form('load', URL+'/data0/'+pid);
    $('#form_sd44').form('clear');
    $('#form_sd44').fo
      $('#main_layout').panel({
        href:URL+'/form/sd44',
        method: 'GET',
        queryParams:{"pid":pid}
})
      //$('#form_sd44').form('load', URL+'/data0/'+pid);
  }
 
    </script>
{{-- http://www.jeasyui.com/forum/index.php?topic=532.0 --}}