<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storedata extends Model
{
   protected $table = 'tb_store_data';
   public $timestamps = false;
}
