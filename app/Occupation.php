<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    //
    protected $table = 'tb_occupation';
    public $timestamps = false;
}
