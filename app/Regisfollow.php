<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regisfollow extends Model
{
    protected $table = 'tb_regis_follow';
    public $timestamps = false;
}
