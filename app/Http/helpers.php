<?php
use App\Office;
use App\District;
class Helpers
{

public static function ENV_DISTRICT_CODE()
{
	$district = Office::select('office_code')->first();
	return $district->office_code;
}
public static function cthai ($num){
  //  $num1 = str_replace('-', ' ', $num);
    return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
	array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
    $num);
}
public static function FnID($var)
{
	$srt[0] = substr($var, 0, 1);
	$srt[1] = substr($var, 1, 4);
	$srt[2] = substr($var, 5, 5);
	$srt[3] = substr($var, 10, 2);
	$srt[4] = substr($var, 12, 1);
	return Helpers::cthai($srt[0]." ".$srt[1]." ".$srt[2]." ".$srt[3]." ".$srt[4]);
}
public static function FnIDEn($var)
{
	$srt[0] = substr($var, 0, 1);
	$srt[1] = substr($var, 1, 4);
	$srt[2] = substr($var, 5, 5);
	$srt[3] = substr($var, 10, 2);
	$srt[4] = substr($var, 12, 1);
	return $srt[0]." ".$srt[1]." ".$srt[2]." ".$srt[3]." ".$srt[4];
}



static function DateTH($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::CmonthShort($birth[1]);
	 $dd = intval($birth[2]);
	 $d = Helpers::cthai($dd);
	 $y = substr($y, 2,4);
	 $yy = Helpers::cthai($y);
	 return $d.' '.$m.' '.$yy;
}
static function DateEn($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::CmonthShort($birth[1]);
	 $d =  intval($birth[2]);
	  $y = substr($y, 2,4);

	 return $d.' '.$m.' '.$y;
}
static function ShortYear($year)
{
	$y = substr($year, 2,4);
	$newyear = Helpers::cthai($y);
	return $newyear;
}
public static function Age($writedate,$birth){
	$w = explode('-',$writedate);

	$b = explode('-',$birth);
	$a= $w[0]-$b[0];
	return Helpers::cthai($a);
}
public static function NumAge($writedate,$birth){
	$w = explode('-',$writedate);

	$b = explode('-',$birth);
	$a= $w[0]-$b[0];
	return  $a;
}

public static function ClassY($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+18;
	return Helpers::cthai($c);
}
public static function Datewrite($var) {
	$w = explode('-',$var);
	$d =explode(' ', $w[2]);
	$day =  intval($d[0]);
	return Helpers::cthai($day);
}
public static function Monthwrite($var) {
	$w = explode('-',$var);
	return Helpers::Cmonth($w[1]);
}
public static function MonthShortwrite($var) {
	$w = explode('-',$var);
	return Helpers::CmonthShort($w[1]);
}
public static function Yearwrite($var) {
	$w = explode('-',$var);
	return Helpers::cthai($w[0]+543);
}
public static function YearShortwrite($var) {
	$w = explode('-',$var);
	$y = substr($w[0]+543, 2,4);
	return Helpers::cthai($y);
}
public static function DateR($var) {
	$w = explode('-',$var);
	$d =explode(' ', $w[2]);
	$day =  intval($d[0]);
	return  $day;
}
public static function MonthR($var) {
	$w = explode('-',$var);
	return Helpers::Cmonth($w[1]);
}
public static function YearR($var) {
	$w = explode('-',$var);
	return  $w[0]+543;
}


public static function Cmonth($var)
{
	switch ($var) {
		case '01':
			return 'มกราคม';
		break;
		case '02':
			return 'กุมภาพันธ์';
		break;
		case '03':
			return 'มีนาคม';
		break;
		case '04':
			return 'เมษายน';
		break;
		case '05':
			return 'พฤษภาคม';
		break;
		case '06':
			return 'มิถุนายน';
		break;
		case '07':
			return 'กรกฎาคม';
		break;
		case '08':
			return 'สิงหาคม';
		break;
		case '09':
			return 'กันยายน';
		break;
		case '10':
			return 'ตุลาคม';
		break;
		case '11':
			return 'พฤศจิกายน';
		break;
		case '12':
			return 'ธันวาคม';
		break;
		 
}

}
public static function CmonthShort($var)
{
	switch ($var) {
		case '01':
			return 'ม.ค.';
		break;
		case '02':
			return 'ก.พ.';
		break;
		case '03':
			return 'มี.ค.';
		break;
		case '04':
			return 'เม.ย.';
		break;
		case '05':
			return 'พ.ค.';
		break;
		case '06':
			return 'มิ.ย.';
		break;
		case '07':
			return 'ก.ค.';
		break;
		case '08':
			return 'ส.ค.';
		break;
		case '09':
			return 'ก.ย.';
		break;
		case '10':
			return 'ต.ค.';
		break;
		case '11':
			return 'พ.ย.';
		break;
		case '12':
			return 'ธ.ค.';
		break;
		 
}

}
public static function Yod($var)
{
	switch ($var){
		case '0':
			return "";
		break;
		case '1':
			return "ร้อยตรี";
		break;
		case '2':
			return "ร้อยโท";
		break;
		case '3':
			return "ร้อยเอก";
		break;
		case '4':
			return "พันตรี";
		break;
		case '5':
			return "พันโท";
		break;
		case '6':
			return "พันเอก";
		break;
		 
	}
}
public static function ArmyPosition($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "สัสดีอำเภอ".$amphur;
		break;
		case '2':
			return "ผู้ช่วยสัสดีอำเภอ".$amphur;
		break;
	}
}
public static function AmphurPosition($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "นายอำเภอ".$amphur;
		break;
		case '2':
			return "ปลัดอำเภอรักษาราชการแทน";
		break;
	}
}
public static function AmphurPositionAddon($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "";
		break;
		case '2':
			return "นายอำเภอ".$amphur;
		break;
	}
}

static function Cidcarddate($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $d = Helpers::cthai($birth[2]);
	 $yy = Helpers::cthai($y);
	 return $d.' '.$m.' '.$yy;
}

public static function Noon1($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+30;
	return Helpers::cthai($c);
}
public static function Noon2($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+40;
	return Helpers::cthai($c);
}
public static function Noon3($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+46;
	return Helpers::cthai($c);
}


public static function GetYear() 
{
			$nowyear = date('Y')+543;
            $begin = $nowyear - 30;
            $end = $nowyear - 17;
            $ii=1;
           		echo "<option>- เลือกปีเกิด/ชั้นปี -</option>";
            for($i=$end;$i>=$begin;$i--)
            {
            	$y =$i+18;
               echo '<option value='.$i.'>เกิด '.$i.' / ชั้นปี '.$y.'</option>';
            }
 }
 public static function GetYearToday() 
{
		 
         
           	$nowyear = date('Y')+543;
            $begin = $nowyear ;
            $end = $nowyear - 5;
       
           		echo "<option>- เลือกปี -</option>";
            for($i=$begin;$i>=$end;$i--)
            {
            	 
               echo '<option value='.$i.'>ปี '.$i.' </option>';
            }
 }
 public static function GetMonth()
 {
 	echo "<option value=\"0\">- เลือกเดือน -</option>";
 	for($i=1;$i<=12;$i++)
 	{
 		echo '<option value='.$i.'>'.Helpers::Cmonth($i).' </option>';
 	}
 }
 public static function GetDistrict()
 {
 			 $district = Office::select('office_amphur')->first();
             $sql = District::select('DISTRICT_CODE','DISTRICT_NAME')->where('AMPHUR_ID',$district->office_amphur)->get();
            		echo "<option value=\"0\">- เลือกตำบล -</option>";
            	foreach ($sql as $data => $d)  
            	{
            		echo "<option value=\"$d->DISTRICT_CODE\"> ".$d->DISTRICT_NAME."</option>";
            	}
 }
  public static function GetDistrictName($district)
 {
 		 
             $sql = District::select('DISTRICT_NAME')->where('DISTRICT_CODE',$district)->first();
             return $sql->DISTRICT_NAME;
            	 
 }


}
