<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
		Route::get('/', function () {
		    return view('frontend.login');
		});
		Route::get('logout','HomeController@getLogout');
		Route::post('login', 'HomeController@postLogin');
		Route::get('main', 'HomeController@getMainwindows');
		Route::get('configsystem', 'HomeController@getConfigsystem');
		Route::get('configuser', 'HomeController@getConfiguser');
		Route::get('configmooban', 'HomeController@getConfigmooban');

		// form
		Route::get('form/{form}', 'FormController@getForm');
		Route::get('showreport/{form}', 'ShowreportController@getForm');
		Route::post('save/{form}', 'FormController@postSave');
		Route::post('edituser/{id}', 'FormController@postEdituser');
		Route::post('editban/{district}/{id}', 'FormController@postEditban');
		Route::post('deleteuser', 'FormController@postDeleteuser');
		Route::post('deleteban', 'FormController@postDeleteban');


		//data
		Route::get('data0/{pid}','DataController@getData0');
		Route::post('data/{type}','DataController@postData');
		Route::post('datazodiac/{year}','DataController@postDatazodiac');

		Route::post('result/{type}','ResultController@postResult');
		Route::post('datathaiperson/{year}/{tumbon}/{status}','ResultController@postThaiperson');
		Route::post('loaddatasd1/{year}/{district}','ResultController@postLoaddatasd1');
		Route::post('loaddatasd1_m16/{year}/{district}','ResultController@postLoaddatasd1_m16');
		Route::post('loaddatasd1_m18/{year}/{month}','ResultController@postLoaddatasd1_m18');
		Route::post('loaddatasd1_m18_month/{year}/{month}','ResultController@postLoaddatasd1_m18_month');
		Route::post('sortsd1/{year}/{district}/{section}','ResultController@postSortsd1');

		Route::post('checkpid','DataController@postCheckpid');
		Route::post('checkthaiperson','DataController@postCheckThaiperson');
		Route::post('countbooksd9','DataController@postCountbooksd9');
		Route::post('dataamphur/{id}','DataController@postDataamphur');
		Route::post('datadistrict/{id}','DataController@postDatadistrict');

		//configprint
		Route::get('configprint/{type}','ConfigprintController@getConfigprint' );
		Route::post('configprint/{type}','ConfigprintController@postConfigprint' );
		
		Route::get('print/{type}','ConfigprintController@getPrint' );
		Route::post('export/{type}','ExportController@postExport' );

		Route::post('search/{type}/{txt_val}','SearchController@postSearch');

		
});
