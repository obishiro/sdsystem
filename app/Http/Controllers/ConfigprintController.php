<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Profile;
use App\User;
use App\Office;
use App\Sd1;
use App\District;
use App\Aumphur;
use App\Province;
use Hash;
use Excel;
use DB;
use Helpers;


class ConfigprintController extends Controller
{
    public function getConfigprint($type)
    {
    	switch ($type)
    	{
    		case 'sd44':
    			$c = DB::table('tb_config_print_sd44')->first();
    			return  view('form.config_print_sd44')->with(array('c'=>$c));
    		break;
            case 'sd1':
                $c = DB::table('tb_config_print_sd1')->first();
                return  view('form.config_print_sd1')->with(array('c'=>$c));
            break;
            case 'sd9':
                $c = DB::table('tb_config_print_sd9')->first();
                return  view('form.config_print_sd9')->with(array('c'=>$c));
            break;
            case 'sd35':
                $c = DB::table('tb_config_print_sd35')->first();
                return  view('form.config_print_sd35')->with(array('c'=>$c));
            break;
    	}
    }
    public function postConfigprint($type,Request $request)
    {
  	  switch ($type)
       {
    	  case 'saveconfigprintsd44':
                $count = DB::table('tb_config_print_sd44')->count();
                if($count<=0)
                {
                    DB::table('tb_config_print_sd44')->insert(
                        [
                            'row1' => $request->input('row1'),
                            'row10' => $request->input('row10'),
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i'),
                            'column_j' => $request->input('column_j'),
                            'column_k' => $request->input('column_k'),
                            'column_l' => $request->input('column_l'),
                            'column_m' => $request->input('column_m'),
                            'column_n' => $request->input('column_n'),
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }else{
                    DB::table('tb_config_print_sd44')
                    ->where('id','1')
                    ->update(
                        [
                            'row1' => $request->input('row1'),
                            'row10' => $request->input('row10'),
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i'),
                            'column_j' => $request->input('column_j'),
                            'column_k' => $request->input('column_k'),
                            'column_l' => $request->input('column_l'),
                            'column_m' => $request->input('column_m'),
                            'column_n' => $request->input('column_n'),
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }
            break;
            case 'saveconfigprintsd35':
                $count = DB::table('tb_config_print_sd35')->count();
                if($count<=0)
                {
                    DB::table('tb_config_print_sd35')->insert(
                        [
                            'row8' => $request->input('row8'),
                            'row10' => $request->input('row10'),
                            'row11' => $request->input('row11'),
                            'row12' => $request->input('row12'),
                            'row13' => $request->input('row13'),
                            'row15' => $request->input('row15'),
                            'row36' => $request->input('row36'),  
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_g' => $request->input('column_g')
                            
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }else{
                    DB::table('tb_config_print_sd35')
                    ->where('id','1')
                    ->update(
                        [
                            'row8' => $request->input('row8'),
                            'row10' => $request->input('row10'),
                            'row11' => $request->input('row11'),
                            'row12' => $request->input('row12'),
                            'row13' => $request->input('row13'),
                            'row15' => $request->input('row15'),
                            'row36' => $request->input('row36'),  
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_g' => $request->input('column_g')
                            
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }
            break;
             case 'saveconfigprintsd1':
                $count = DB::table('tb_config_print_sd1')->count();
                if($count<=0)
                {
                    DB::table('tb_config_print_sd1')->insert(
                        [
                            'row1' => $request->input('row1'),
                            'row11' => $request->input('row11'),
                            'row13' => $request->input('row13'),
                            'row14' => $request->input('row14'),
                            'row16' => $request->input('row16'),
                            'row17' => $request->input('row17'),
                            'row18' => $request->input('row18'),
                            'row19' => $request->input('row19'),
                            'row22' => $request->input('row22'),
                            'row27' => $request->input('row27'),
                            'row29' => $request->input('row29'),
                            'row30' => $request->input('row30'),
                            'row32' => $request->input('row32'),
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i')
                             
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }else{
                    DB::table('tb_config_print_sd1')
                    ->where('id','1')
                    ->update(
                        [
                            'row1' => $request->input('row1'),
                            'row11' => $request->input('row11'),
                            'row13' => $request->input('row13'),
                            'row14' => $request->input('row14'),
                            'row16' => $request->input('row16'),
                            'row17' => $request->input('row17'),
                            'row18' => $request->input('row18'),
                            'row19' => $request->input('row19'),
                            'row22' => $request->input('row22'),
                            'row27' => $request->input('row27'),
                            'row29' => $request->input('row29'),
                            'row30' => $request->input('row30'),
                            'row32' => $request->input('row32'),
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i')
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }
            break;
              case 'saveconfigprintsd9':
                $count = DB::table('tb_config_print_sd9')->count();
                if($count<=0)
                {
                    DB::table('tb_config_print_sd9')->insert(
                        [
                            'row1' => $request->input('row1'),
                            'row6' => $request->input('row6'),
                          //  'row16' => $request->input('row16'),
                            'row18' => $request->input('row18'),
                           
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i'),
                            'column_j' => $request->input('column_j'),
                            'year' => $request->input('year_dob')
                             
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }else{
                    DB::table('tb_config_print_sd9')
                    ->where('id','1')
                    ->update(
                        [
                            'row1' => $request->input('row1'),
                            'row6' => $request->input('row6'),
                            
                        //    'row16' => $request->input('row16'),
                            'row18' => $request->input('row18'),
                           
                            'allrow' => $request->input('allrow'),
                            'column_a' => $request->input('column_a'),
                            'column_b' => $request->input('column_b'),
                            'column_c' => $request->input('column_c'),
                            'column_d' => $request->input('column_d'),
                            'column_e' => $request->input('column_e'),
                            'column_f' => $request->input('column_f'),
                            'column_g' => $request->input('column_g'),
                            'column_h' => $request->input('column_h'),
                            'column_i' => $request->input('column_i'),
                            'column_j' => $request->input('column_j'),
                            'year' => $request->input('year_dob')
                        ]);
                     $result = array('status'=>1);
                    return json_encode($result);
                }
            break;
            
      }
   
}
}
