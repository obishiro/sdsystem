<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Profile;
use App\User;
use App\Office;
use Hash;
use App\Sd1;
use DB;


class ShowreportController extends Controller
{
    public function getForm($type)
    {
    	if(Auth::check()){
    	switch($type):
    		
            case 'sd1':
                return view('result.sd1');
            break;
            case 'sd1_m16':
                return view('result.sd1_m16');
            break;
            case 'sd1_m18':
                return view('result.sd1_m18');
            break;
            

    	endswitch;
    }else{
    		return redirect('/');
    }
    }
}