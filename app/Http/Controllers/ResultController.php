<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Profile;
use App\Sd1;
use App\Mooban;
use Helpers;
use App\Thaiperson;
use DB;

class ResultController extends Controller
{
   	public function postResult($type)
   	{
   		if(Auth::check())
   		{
   			switch($type):
   				case 'profile':
                  $profile_data = Profile::select('tb_profile.pid','tb_profile.sol_id','tb_prefix.prefix_name','tb_profile.firstname','tb_profile.lastname','tb_position.position_name')
                  ->join('tb_prefix','tb_prefix.prefix_no','=','tb_profile.prefix')
                  ->join('tb_position','tb_position.position_no','=','tb_profile.position')
                  ->orderBy('tb_profile.id','desc')->get();
                  return $profile_data;
   				break;
          case 'mooban':
                $bandata = Mooban::select('tb_mooban.district','tb_district.DISTRICT_NAME','tb_mooban.moo','tb_mooban.banname','tb_mooban.bossname','tb_mooban.phonenumber')
                ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_mooban.district')
                ->orderBy('tb_mooban.district','asc')
                ->orderBy('tb_mooban.moo','asc')
                ->get();
                return $bandata;
          break;

               case 'sd1':
                  $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')->get();
                  $c= Sd1::count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';
             
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
               break;
            case 'sd1_m16':
                  $year_today = date('Y');
                  $year = $year_today -17;
                  $month = date('m');
                  $year_regis = date('Y');
                  $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  //->orderBy(DB::raw('tb_sd1.district asc ,tb_sd1.moo asc,tb_sd1.home_id * 1 '))
                  ->orderBy('tb_sd1.sd9_no','desc')
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->whereRaw("YEAR(date_regis) = '$year_regis'" )
                  ->whereRaw("MONTH(date_regis) = '$month'" )

                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->orderBy(DB::raw('tb_sd1.sd9_no '))
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->whereRaw("YEAR(date_regis) = '$year_regis'" )
                  ->whereRaw("MONTH(date_regis) = '$month'" )

                  ->count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';
             
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
               break;
                case 'sd1_m18':
                  $month = date('m');
                  $year = date('Y');
                  $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  //->orderBy(DB::raw('tb_sd1.district asc ,tb_sd1.moo asc,tb_sd1.home_id * 1 '))
                  ->where('tb_sd1.section','18')
                  ->orderBy('tb_sd1.date_regis','asc')
                  ->whereRaw("YEAR(date_regis) = '$year'" )
                  ->whereRaw("MONTH(date_regis) = '$month'" )

                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.section','18')
                  ->orderBy(DB::raw('tb_sd1.date_regis'))
                  ->whereRaw("YEAR(date_regis) = '$year'" )
                  ->whereRaw("MONTH(date_regis) = '$month'" )
                  ->count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';
             
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
               break;
   			endswitch;	
   		}else{
   			return redirect('/');
   		}
   	}
   public function postLoaddatasd1($year,$district)
   {
     $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '$year'" )
                   ->orderBy('sd1_no','asc')
                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';

               
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
   }
 public function postLoaddatasd1_m16($year,$district)
   {
    if($district=="0")
    {
     $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
               
                  ->where('tb_sd1.section','=','16')
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '$year'" )
                   ->orderBy('sd9_no','desc')
                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->orderBy('sd9_no','desc')
                  ->where('tb_sd1.section','=','16')
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->count();
                }else{
                  $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
                  ->where('tb_sd1.section','=','16')
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '$year'" )
                   ->orderBy('sd1_no','asc')
                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
                  ->where('tb_sd1.section','=','16')
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->orderBy('sd1_no','asc')
                  ->count();
                }
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';

               
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
    
   }
public function postLoaddatasd1_m18($year,$month)
   {
     $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
              
                  ->where('tb_sd1.section','=','18')
          
                   ->whereRaw("YEAR(tb_sd1.date_regis) = '$year'" )
                  ->whereRaw("MONTH(tb_sd1.date_regis) = '$month'" )
                   ->orderBy('tb_sd1.date_regis','asc')
                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                
                  ->where('tb_sd1.section','=','18')
                   ->whereRaw("YEAR(date_regis) = '$year'" )
                  ->whereRaw("MONTH(date_regis) = '$month'" )
                  ->count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$s->sd1_no.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';

               
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
   }
   // public function postLoaddatasd1_m18_month($year,$month)
   // {
   //   $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
   //                ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
   //              //  ->where('tb_sd1.district','=',$district)
   //                ->where('tb_sd1.section','=','18')
   //            //    ->where(DB::raw("YEAR(dob) = '1997'"))
   //                 ->whereRaw("YEAR(date_regis) = '$year'" )
   //                 ->whereRaw("MONTH(date_regis = '$month'" )
   //                 ->orderBy('sd1_no','asc')
   //                ->get();
   //                $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
   //                ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                 
   //                ->where('tb_sd1.section','=','18')
   //                   ->whereRaw("YEAR(date_regis) = '$year'" )
   //                 ->whereRaw("MONTH(date_regis = '$month'" )
   //                ->count();
             
   //           $i=1;
   //           $data = '[';
   //            foreach ($sql as $sqldata=>$s)
   //            {
   //              $data .= '{';
   //              $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
   //              $data.=',"sd1_no":"'.$s->sd1_no.'"';
   //              $data.=',"sd9_no":"'.$s->sd9_no.'"';
   //              $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
   //              $data.=',"firstname":"'.$s->firstname.'"';
   //              $data.=',"lastname":"'.$s->lastname.'"';
   //              $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
   //              $data.=',"age":"'.$s->age.'"';
   //              $data.=',"home_id":"'.$s->home_id.'"';
   //              $data.=',"moo":"'.$s->moo.'"';
   //              $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
   //              $data.=',"fa_name":"'.$s->fa_name.'"';
   //              $data.=',"fa_lname":"'.$s->fa_lname.'"';
   //              $data.=',"ma_name":"'.$s->ma_name.'"';
   //              $data.=',"ma_lname":"'.$s->ma_lname.'"';
   //              $data.=',"section":"'.$s->section.'"';

               
   //              $data .="}";
   //              if($i<$c){
   //                  $data.=",";
   //                  }
   //                  $i++;
   //            }
   //            $data.=']';
   //          return $data;
   // }
      public function postSortsd1($year,$district,$section)
   {
      switch($section):
        case '16':
          $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
                  ->where('tb_sd1.section','=',$section)
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '$year'" )
                   ->orderBy(DB::raw('moo asc,home_id * 1 '))
                  
                  ->get();
                  $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->where('tb_sd1.district','=',$district)
                  ->where('tb_sd1.section','=',$section)
                  ->whereRaw("YEAR(dob) = '$year'" )
                  ->count();
                 
        break;
        case '18':
      // $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
      //             ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
      //             ->where('tb_sd1.district','=',$district)
      //             ->where('tb_sd1.section','=',$section)
      //         //    ->where(DB::raw("YEAR(dob) = '1997'"))
      //              ->whereRaw("YEAR(dob) = '$year'" )
      //              ->orderBy(DB::raw('moo asc,home_id * 1 '))
                  
      //             ->get();
      //             $c= Sd1::select('tb_sd1.date_regis','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
      //             ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
      //             ->where('tb_sd1.district','=',$district)
      //             ->where('tb_sd1.section','=',$section)
      //             ->whereRaw("YEAR(dob) = '$year'" )
      //             ->count();
      //             $i=1;
                          break;
      endswitch;
     
             
              $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                Sd1::where('pid',$s->pid)->update(['sd1_no' => $i]);
                $data .= '{';
                $data .='"date_regis":"'.Helpers::DateEN($s->date_regis).'"';
                $data.=',"sd1_no":"'.$i.'"';
                $data.=',"sd9_no":"'.$s->sd9_no.'"';
                $data.=',"pid":"'.Helpers::FnIDEn($s->pid).'"';
                $data.=',"firstname":"'.$s->firstname.'"';
                $data.=',"lastname":"'.$s->lastname.'"';
                $data.=',"dob":"'.Helpers::DateEN($s->dob).'"';
                $data.=',"age":"'.$s->age.'"';
                $data.=',"home_id":"'.$s->home_id.'"';
                $data.=',"moo":"'.$s->moo.'"';
                $data.=',"DISTRICT_NAME":"'.$s->DISTRICT_NAME.'"';
                $data.=',"fa_name":"'.$s->fa_name.'"';
                $data.=',"fa_lname":"'.$s->fa_lname.'"';
                $data.=',"ma_name":"'.$s->ma_name.'"';
                $data.=',"ma_lname":"'.$s->ma_lname.'"';
                $data.=',"section":"'.$s->section.'"';

               
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
   }
   public function postThaiperson($year,$district,$status)
   {
    if($district!=0)
    {
    $txt = str_split($district);
    $prov = $txt[0].''.$txt[1];
    $aumphur =$txt[2].''.$txt[3];
    $tumbon = $txt[4].''.$txt[5];
    }

      if($district!=0 && $status!=9) //choose district & status
      {
        switch($status)
        {
          case '0':
            $strStatus=NULL;
          break;
          case '1':
            $strStatus=1;
          break;
        }
           
      $sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.STATUS','tb_data0.CC','tb_data0.AA','tb_data0.TT')
      ->where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,
        'CC'=>$prov,
        'AA'=>$aumphur,
        'TT'=>$tumbon,
        'STATUS'=>$strStatus
      ))->orderBy(DB::raw('TT asc,MM asc,ADDR * 1 '))->get();
      $c= Thaiperson::where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,
        'CC'=>$prov,
        'AA'=>$aumphur,
        'TT'=>$tumbon,
        'STATUS'=>$strStatus
      ))->count();
    }else if ($district==0 && $status!=9) //choose year & status
    {
       switch($status)
        {
          case '0':
            $strStatus=NULL;
          break;
          case '1':
            $strStatus=1;
          break;
        }
       $sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.STATUS','tb_data0.CC','tb_data0.AA','tb_data0.TT')
      ->where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,       
        'STATUS'=>$strStatus
      ))->orderBy(DB::raw('TT asc,MM asc,ADDR * 1 '))->get();
      $c= Thaiperson::where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,
        'STATUS'=>$strStatus
      ))->count();
    }else  if ($district==0 && $status==9) //choose year & status
    {
       $sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.STATUS','tb_data0.CC','tb_data0.AA','tb_data0.TT')
      ->where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year
      ))->orderBy(DB::raw('TT asc,MM asc,ADDR * 1 '))->get();
      $c= Thaiperson::where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year
      ))->count();
    }else if($district!=0 && $status==9) //choose district & status
      {
        
           
      $sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.STATUS','tb_data0.CC','tb_data0.AA','tb_data0.TT')
      ->where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,
        'CC'=>$prov,
        'AA'=>$aumphur,
        'TT'=>$tumbon
      ))->orderBy(DB::raw('TT asc,MM asc,ADDR * 1 '))->get();
      $c= Thaiperson::where(array(
        'RCODE'=>Helpers::ENV_DISTRICT_CODE(),
        'DOB_Y'=>$year,
        'CC'=>$prov,
        'AA'=>$aumphur,
        'TT'=>$tumbon
      ))->count();
    }

      // $tumbon_name = DB::table('tb_district')->select('DISTRICT_NAME')->where('DISTRICT_CODE','=',$district)->first();
      $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                if($s->STATUS=="" || $s->STATUS==NULL || $s->STATUS=="0")
                {
                  $state = '0';
                }else{
                  $state = '1';
                }
                $tumbon_code = $s->CC.''.$s->AA.''.$s->TT;
                $y = $s->DOB_Y - 543;
                $dob = $y.'-'.$s->DOB_M.'-'.$s->DOB_D;
                $data .= '{';
                $data.='"pid":"'.Helpers::FnIDEn($s->PID).'"';
                $data.=',"firstname":"'.$s->FNAME.'"';
                $data.=',"lastname":"'.$s->LNAME.'"';
                $data.=',"dob":"'.Helpers::DateEN($dob).'"';
                $data.=',"faname":"'.$s->FANAME.'"';
                $data.=',"maname":"'.$s->MANAME.'"';
                $data.=',"addr":"'.$s->ADDR.'"';
                $data.=',"moo":"'.$s->MM.'"';
                $data.=',"tumbon":"'.Helpers::GetDistrictName($tumbon_code).'"';
                $data.=',"links":" '.$state.'"';


                  $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
   }

}
