<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Profile;
use App\Office;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
    	$username = str_replace('-','',$request->input('txt_name'));
    	$password = $request->input('txt_password');
    	
       if (Auth::attempt(['username' => $username, 'password' => $password])) 
       {
        return '1';
        $lifetime = time() + 60 * 60 * 12; // one year
        Config::set('session.lifetime', $lifetime);
       }else{
       	return '0';
       }
    }
    public function getMainwindows(Request $request)
    {
      if(Auth::check()){
      $sql_user = Profile::select('tb_profile.firstname','tb_profile.lastname','tb_prefix.prefix_name','tb_position.position_name')  
      ->join('tb_prefix','tb_prefix.prefix_no','=','tb_profile.prefix')
      ->join('tb_position','tb_position.position_no','=','tb_profile.position')
      ->where('tb_profile.pid',Auth::user()->username)
      ->first();
    	return view('frontend.main_windows')
    		->with(array('u'=> $sql_user,'header_menu'=>'ระบบสารสนเทศสายงานสัสดี เวอร์ชั่น ๐.๑'));
      }else{
        return redirect('/');
      }
    }
   public function getConfigsystem(Request $request)
    {
      if(Auth::check()){
        $count_office = Office::count();
        if($count_office<=0)
        {
          $data = array(
            'office_name'=>'','office_shortname'=>'','office_no'=>'','office_soldier_no'=>'',
            'road'=>'','trok'=>'','soy'=>'','office_home_id'=>'','office_moo'=>'','office_tel'=>''
            );
        }else{
          $o = Office::first();
          $data = array(
            'office_name'=>$o->office_name,'office_shortname'=>$o->office_shortname,'office_no'=>$o->office_no,'office_soldier_no'=>$o->office_soldier_no,
            'road'=>$o->road,'trok'=>$o->trok,'soy'=>$o->soy,'office_home_id'=>$o->office_home_id,'office_moo'=>$o->office_moo,'office_tel'=>$o->office_tel
            );
        }
      return view('form.config_system')
        ->with($data);
      }else{
        return redirect('/');
      }
    }
     public function getConfiguser(Request $request)
    {
      if(Auth::check()){
      return view('form.config_user')
        ->with(array());
      }else{
        return redirect('/');
      }
    }
    public function getConfigmooban(Request $request)
    {
      if(Auth::check()){
      return view('form.config_mooban')
        ->with(array());
      }else{
        return redirect('/');
      }
    }
    public function getLogout()
    {
    	Auth::logout();
    	return redirect('/');
    }

}
