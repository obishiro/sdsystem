<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Storedata;
use App\Religion;
use App\Education;
use App\Occupation;
use App\Regisfollow;
use App\Scar;
use App\Zodiac;
use App\District;
use App\Aumphur;
use App\Province;
use App\Position;
use App\Prefix;
use App\Sd1;
use App\Office;
use App\Thaiperson;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
 
class DataController extends Controller
{
    public function getData0($txtpid)
    {
    	 
    	// 	case 'loaddb':
    	// 		$count = Storedata::count();
    	// 		if ($count <= 0)
    	// 		{
     //             return '0';
    	// 		}else{
     //             $bd=""   ;
    	// 		 $users =Storedata::first();
				 // $name_encrypt =str_replace('   ','',$users->name);
     //             $name_decode = explode('Mr.',$name_encrypt);
     //             $replace_title = str_replace('นาย ','', $name_decode[0]);
     //             $n = explode(' ',$replace_title);
     //             $name  = $n[0];
     //             $lname = $n[2];
     //             preg_match_all('/\d+/', $name_encrypt, $matches);
     //             $date = $users->dob;
     //             $d = explode('/', $date);
     //             // foreach($date as $dates =>$d){
     //             //    $bd = $d;
     //             // }
     //             // $dob =  substr_replace($bd ,"",-1);
     //             $year = $d[2];
     //             $day = $d[0];
     //             // $mm = substr_replace($dob, "",0,4);
     //             $month = $d[1];
     //             $nowyear = date('Y')+543;
     //             $age = $nowyear - $year;
     //             $eng_year = $year -543;
     //             $date_issue = str_replace('/','-',$users->date_issue);
                
     //             $array =array(
     //                'pid'=>$users->cid,'name'=>$name,'lname'=>$lname,'day'=>$day,'month'=>$month,
     //                'year'=>$year,'fa_lname'=>$lname,'ma_lname'=>$lname,'age'=>$age,'date_issue'=>$date_issue
     //                );
     //             return json_encode($array);
    	// 		}
        // 	break;

        $pid= str_replace('-','',$txtpid);
        $numrows= Thaiperson::where('PID',$pid)->count();
        if($numrows>0)
        {
        $sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.CC','tb_data0.AA','tb_data0.TT')->where('PID',$pid)->first();
        $data =array(
            'pid'=>$sql->PID,
            'name'=>$sql->FNAME,
            'lname'=>$sql->LNAME,
            'fa_name'=>$sql->FANAME,
            'ma_name'=>$sql->MANAME ,
            'fa_lname'=>$sql->LNAME,
            'ma_lname'=>$sql->LNAME,
            'date_dob'=>$sql->DOB_D,
            'month_dob'=>$sql->DOB_M,
            'year_dob'=>$sql->DOB_Y,
            'homeid'=>$sql->ADDR,
            'moo'=>$sql->MM,
            'district'=>$sql->CC.''.$sql->AA.''.$sql->TT

        );
           
         return json_encode($data);
    }else{
        return json_encode(array('val'=>0));
    }
    }
    public function postData($type)
    {
        switch($type)
        {
            case 'date':
                $data = '[';
                for($i=1;$i<=31;$i++) 
                {
                        $data .= '{';
                        $data .='"id":'.$i.',"text":'.$i."";
                        $data .="}";
                            if($i<31){
                            $data.=",";
                             }
                }
                $data.=']';
                return $data;
            break;
            case 'month':
                $thai_month_arr=array(  
                    "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"                    
                    ); 
                $data = '[';
                for($i=0;$i<=11;$i++)
                    {
                        $ii =$i+1;
                        $data .= '{';
                        $data .='"id":'.$ii.',"text":';
                        $data .='"'.$thai_month_arr[$i].'"';
                        $data .="}";
                            if($i<11){
                            $data.=",";
                             }
                    }
                 $data.=']';
                return $data;
            break;

        case 'date_regis':
                $date = date('d');
                $data = '[';
                for($i=1;$i<=31;$i++) 
                {
                        $data .= '{';
                        $data .='"id":'.$i.',"text":'.$i."";
                        if($date ==$i){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                    }else{
                        $data.="";
                    }
                        $data .="}";
                            if($i<31){
                            $data.=",";
                             }
                }
                $data.=']';
                return $data;
            break;
            case 'month_regis':
                $month = date('m');
                if($month < 10){
                    $m = str_replace('0','',$month);
                }else{
                    $m = $month;
                }
                $thai_month_arr=array(  
                    "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"                    
                    ); 
                $data = '[';
                for($i=0;$i<=11;$i++)
                    {
                        $ii =$i+1;
                        $data .= '{';
                        $data .='"id":'.$ii.',"text":';
                        $data .='"'.$thai_month_arr[$i].'"';
                        if($m==$ii){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                        }else{
                        $data.="";
                        }
                        $data .="}";
                            if($i<11){
                            $data.=",";
                             }
                    }
                 $data.=']';
                return $data;
            break;
        case 'date_issue':
                $date = date('d');
                $data = '[';
                for($i=1;$i<=31;$i++) 
                {
                        $data .= '{';
                        $data .='"id":'.$i.',"text":'.$i."";
                        if($date ==$i){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                    }else{
                        $data.="";
                    }
                        $data .="}";
                            if($i<31){
                            $data.=",";
                             }
                }
                $data.=']';
                return $data;
            break;
            case 'month_issue':
                $month = date('m');
                if($month < 10){
                    $m = str_replace('0','',$month);
                }else{
                    $m = $month;
                }
                $thai_month_arr=array(  
                    "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"                    
                    ); 
                $data = '[';
                for($i=0;$i<=11;$i++)
                    {
                        $ii =$i+1;
                        $data .= '{';
                        $data .='"id":'.$ii.',"text":';
                        $data .='"'.$thai_month_arr[$i].'"';
                        if($m==$ii){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                        }else{
                        $data.="";
                        }
                        $data .="}";
                            if($i<11){
                            $data.=",";
                             }
                    }
                 $data.=']';
                return $data;
            break;
        case 'year':
            $nowyear = date('Y')+543;
            $begin = $nowyear - 30;
            $end = $nowyear - 17;
            $ii=1;
            $data = '[';
            for($i=$end;$i>=$begin;$i--)
            {
                $data .= '{';
                $data .='"id":'.$i.',"text":';
                $data.=$i;
                $data .="}";

                if($i>$begin){
                    $data.=",";
                    }
                    $ii++;
            }
            $data.=']';
            return $data;
           
        break;
        case 'religion':
             $sql = Religion::get();
             $c= Religion::count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"religion_no":'.$s->religion_no.',"religion_name":';
                $data .='"'.$s->religion_name.'"';

                 if($s->religion_no=="1"){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                    }else{
                        $data.="";
                    }
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
        break;
        case 'education':
             $sql = Education::all();
             return $sql;
        break;
        case 'occupation':
             $sql = Occupation::all();
             return $sql;
        break;
        case 'regisfollow':
             $sql = Regisfollow::all();
             //return $sql;
             $c= Regisfollow::count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"rf_no":'.$s->rf_no.',"rf_name":';
                $data .='"'.$s->rf_name.'"';

                 if($s->rf_no=="1"){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                    }else{
                        $data.="";
                    }
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
        break;
        case 'scar':
             $sql = Scar::all();
             return $sql;
        break;
        case 'district':
             //$district = 280;
             $district = Office::select('office_amphur')->first();
             $sql = District::select('DISTRICT_CODE','DISTRICT_NAME')->where('AMPHUR_ID',$district->office_amphur)->get();
             return $sql;
        break;
        case 'aumphur':
             //$amphur = 280;
              $district = Office::select('office_amphur')->first();
             $sql =Aumphur::select('AMPHUR_CODE','AMPHUR_NAME')->where('AMPHUR_ID',$district->office_amphur)->get();
             $s = explode('}',$sql);
             $str = $s[0].',"selected":true}'.$s[1];
             return $str;
        break;
        case 'province':
           //  $province = 21;
             $prov = Office::select('office_province')->first();
             $sql =Province::select('PROVINCE_CODE','PROVINCE_NAME')->where('PROVINCE_ID',$prov->office_province)->get();
             $s = explode('}',$sql);
             $str = $s[0].',"selected":true}'.$s[1];
             return $str;
        break;

        case 'allprovince':
           
             $sql =Province::select('PROVINCE_ID','PROVINCE_NAME')->orderBy('PROVINCE_NAME','asc')->get();
             return $sql;
        break;
         case 'position':
             $sql = Position::get();
             $c= Position::count();
             
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"position_no":'.$s->position_no.',"position_name":';
                $data .='"'.$s->position_name.'"';

                 if($s->position_no=="1"){
                       //  $data. = ',"selected":true';
                        $data .=',"selected":true';
                    }else{
                        $data.="";
                    }
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
        break;
    case 'prefix':
             $sql = Prefix::get();
             $c= Prefix::count();
             $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                $data .= '{';
                $data .='"prefix_no":'.$s->prefix_no.',"prefix_name":';
                $data .='"'.$s->prefix_name.'"';
                $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
        break;

    }
    }
    public function postDatazodiac($year)
    {
         $y = $year-543;
         $arr=array("1","2","3","4","5","6","7","8","9","10","11","12");
         $yy = $arr[($y-1)%12] ;
         $sql = Zodiac::where('zodiac_no',$yy)->get(); 
         $s = explode('}',$sql);
         $str = $s[0].',"selected":true}'.$s[1];
         return $str;
         
    }
    public function postCheckpid(Request $request)
    {
        $pid = str_replace('-','',$request->input('pid'));
        $count = Sd1::where('pid',$pid)->count();
        if($count<=0)
        {
            $data = array('status'=>'0');
        }else{
            $data = array('status'=>'1');
        }
        return json_encode($data);

    }
    public function postCheckThaiperson(Request $request)
    {
        $pid = str_replace('-','',$request->input('pid'));
        $count = Thaiperson::where('PID',$pid)->count();
        if($count<=0)
        {
            $data = array('status'=>'0');
        }else{
            $data = array('status'=>'1');
        }
        return json_encode($data);

    }
    public function postDataamphur($id)
    {
        $sql =Aumphur::select('AMPHUR_ID','AMPHUR_NAME')->where('PROVINCE_ID',$id)->orderBy('AMPHUR_NAME','asc')->get();
        return $sql;

    }
    public function postDatadistrict($id)
    {
       $sql = District::select('DISTRICT_CODE','DISTRICT_NAME')->where('AMPHUR_ID',$id)->orderBy('DISTRICT_NAME','asc')->get();
       return $sql;
    }
            
    public function postCountbooksd9 (Request $request)
    {
        $number = $request->input('number');
        if($number <=100){
            $number_book ='1';
        }else if($number >100 && $number <=200){
            $number_book ='2';
        }else if($number >200 && $number <=300){
            $number_book ='3';
        }else if($number >300 && $number <=400){
            $number_book ='4';
        }else if($number >400 && $number <=500){
            $number_book ='5';
        }else if($number >500 && $number <=600){
            $number_book ='6';
        }else if($number >600 && $number <=700){
            $number_book ='7';
        }else if($number >700 && $number <=800){
            $number_book ='8';
        }else if($number >800 && $number <=900){
            $number_book ='9';
        }else if($number >900 && $number <=1000){
            $number_book ='10';
        }else if($number >1000 && $number <=1100){
            $number_book ='11';
        }else if($number >1100 && $number <=1200){
            $number_book ='12';
        }else if($number >1200 && $number <=1300){
            $number_book ='13';
        }else if($number >1300 && $number <=1400){
            $number_book ='14';
        }else if($number >1400 && $number <=1500){
            $number_book ='15';
        }
        $result = array('status'=>true,'number'=>$number_book);
        return json_encode($result);
  



    }          
       


 
}
