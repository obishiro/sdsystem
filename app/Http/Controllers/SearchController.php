<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

use Helpers;
use App\Thaiperson;
use DB;
 
class SearchController extends Controller
{
    public function postSearch($type,$txt_val)
    {
    	if(Auth::check())
   		{
    	switch ($type) {
    		case 'thaiperson':
    			$sql = Thaiperson::select('tb_data0.PID','tb_data0.FNAME','tb_data0.LNAME','tb_data0.DOB_D','tb_data0.DOB_M','tb_data0.DOB_Y','tb_data0.FANAME','tb_data0.MANAME','tb_data0.ADDR','tb_data0.MM','tb_data0.STATUS','tb_data0.CC','tb_data0.AA','tb_data0.TT')
			     	 ->where('RCODE','=',Helpers::ENV_DISTRICT_CODE())
			     	 ->where(function ($query) use ($txt_val) {
                			$query->where('PID', 'LIKE', "%$txt_val%")
                      		->orWhere('FNAME', 'LIKE', "%$txt_val%")
                      		->orWhere('LNAME','LIKE', "%$txt_val%")
                      		->orWhere('FANAME','LIKE', "%$txt_val%")
                      		->orWhere('MANAME','LIKE', "%$txt_val%")
                      		;
           				 })
			     	->orderBy(DB::raw('TT asc,MM asc,ADDR * 1 '))->get();

			      	$c= Thaiperson::where('RCODE','=',Helpers::ENV_DISTRICT_CODE())
			      	->where(function ($query) use($txt_val) {
                			$query->where('PID', 'LIKE', "%$txt_val%")
                      		->orWhere('FNAME', 'LIKE', "%$txt_val%")
                      		->orWhere('LNAME','LIKE', "%$txt_val%")
                      		->orWhere('FANAME','LIKE', "%$txt_val%")
                      		->orWhere('MANAME','LIKE', "%$txt_val%");
           				 })
			      	->count(); 
			      	  $i=1;
             $data = '[';
              foreach ($sql as $sqldata=>$s)
              {
                if($s->STATUS=="" || $s->STATUS==NULL || $s->STATUS=="0")
                {
                  $state = '0';
                }else{
                  $state = '1';
                }
                $tumbon_code = $s->CC.''.$s->AA.''.$s->TT;
                $y = $s->DOB_Y - 543;
                $dob = $y.'-'.$s->DOB_M.'-'.$s->DOB_D;
                $data .= '{';
                $data.='"pid":"'.Helpers::FnIDEn($s->PID).'"';
                $data.=',"firstname":"'.$s->FNAME.'"';
                $data.=',"lastname":"'.$s->LNAME.'"';
                $data.=',"dob":"'.Helpers::DateEN($dob).'"';
                $data.=',"faname":"'.$s->FANAME.'"';
                $data.=',"maname":"'.$s->MANAME.'"';
                $data.=',"addr":"'.$s->ADDR.'"';
                $data.=',"moo":"'.$s->MM.'"';
                $data.=',"tumbon":"'.Helpers::GetDistrictName($tumbon_code).'"';
                $data.=',"links":" '.$state.'"';


                  $data .="}";
                if($i<$c){
                    $data.=",";
                    }
                    $i++;
              }
              $data.=']';
            return $data;
    		break;
    		
    	 
    	}
    }else{
   			return redirect('/');
   		}
	}
}
