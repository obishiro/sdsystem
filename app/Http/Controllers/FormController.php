<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Profile;
use App\User;
use App\Office;
use App\Mooban;
use Hash;
use File;
use App\Sd1;
use DB;
use Illuminate\Support\Facades\Storage;


class FormController extends Controller
{
    public function getForm($type)
    {
    	if(Auth::check()){
    	switch($type):
    		case 'sd44':
                $office = Office::select('tb_amphur.AMPHUR_NAME')
                ->join('tb_amphur','tb_amphur.AMPHUR_ID','=','tb_office.office_amphur')
                ->first();
    			return view('form.sd44',['office'=>$office]);
                //return $off
    		break;
          
            case 'sortsd1':
                return view('form.sortsd1');
            break;
            case 'sd35year':
                return view('form.sd35year');
            break;
            case 'thaiperson':
                return view('result.thaiperson');
            break;
            case 'formthaiperson':
            
            $manuals = [];
            $filesInFolder = \File::files('data0');

            foreach($filesInFolder as $path)
            {
                $manuals[] = pathinfo($path);
            }
            
     
                return view('form.thaiperson')->with('files', $manuals);
            break;

    	endswitch;
    }else{
    		return redirect('/');
    }
    }
    public function postSave(Request $request,$type)
    {
    	if(Auth::check()){
    	switch($type):
    		case 'sd44':
                $pid = str_replace('-','',$request->input('pid'));
                $pcard_id = str_replace('-','',$request->input('pcard_id'));
                $day = $request->input('date_regis');
                $month = $request->input('month_regis');
                $year = $request->input('year_regis')-543;
                $day_dob = $request->input('date_dob');
                $month_dob = $request->input('month_dob');
                $year_dob = $request->input('year_dob')-543;
                $day_issue = $request->input('date_issue');
                $month_issue = $request->input('month_issue');
                $year_issue = $request->input('year_issue')-543;
                $count = Sd1::where('pid',$pid)->count();

                if($count <= 0) {
                    if($request->input('age') > 17)
                    {
                        $section ='18';
                    }else{
                        $section ='16';
                    }
                $s = new Sd1;
               
                $s->date_regis = $year.'-'.$month.'-'.$day;
                $s->pid = $pid;
                $s->firstname = $request->input('name');
                $s->lastname = $request->input('lname');
                $s->dob = $year_dob.'-'.$month_dob.'-'.$day_dob;
                $s->age = $request->input('age');
                $s->zodiac_id = $request->input('zodiac');
                $s->scar_id = $request->input('scar');
                $s->nationality_id = $request->input('nationality');
                $s->religion_id = $request->input('religion');
                $s->education_id = $request->input('education');
                $s->occupation_id = $request->input('occupation');
                $s->regisfollow_id = $request->input('regisfollow');
                $s->fa_name = $request->input('fa_name');
                $s->fa_lname = $request->input('fa_lname');
                $s->ma_name = $request->input('ma_name');
                $s->ma_lname= $request->input('ma_lname');
                $s->fa_nationality= $request->input('fa_nationality');
                $s->road = $request->input('road');
                $s->trok = $request->input('trok');
                $s->soy = $request->input('soy');
                $s->home_id = $request->input('homeid');
                $s->moo = $request->input('moo');
                $s->district = $request->input('district');
                $s->amphur = $request->input('aumphur');
                $s->province = $request->input('province');
                $s->sd1_no = $request->input('sd1_no');
                $s->sd9_no = $request->input('sd9_no');
                $s->sd9_book = $request->input('sd9_book');
                $s->pcard_id = $request->input('pcard_id');
                $s->pcard_by = $request->input('pcard_by');
                $s->pcard_issue = $year_issue.'-'.$month_issue.'-'.$day_issue;
                $s->author = $request->input('position');
                $s->kadee = $request->input('kadee');
                $s->section = $section;
                $s->add_by = Auth::user()->username;
                $s->created_at = date('Y-m-d H:i:s');
                $s->updated_at = date('Y-m-d H:i:s');
                
                if($s->save())
                {
                    $result = array('status'=>1,'age'=>$request->input('age'));
                }else{
                    $result = array('errorMsg'=>'ไม่สามารถบันทึกข้อมูลได้');
                }
    			 
    			     return $result;
                }else{
                    return $result =array('status'=>2);
                }

    		break;
            case 'profile':
                $pid = str_replace('-','',$request->input('pid'));
                $soldier_id = str_replace('-','',$request->input('sol_id'));
                $profile = new Profile;
                $profile->pid =  $pid;
                $profile->sol_id =  $soldier_id;
                $profile->prefix =  $request->input('prefix');
                $profile->firstname =  $request->input('firstname');
                $profile->lastname =  $request->input('lastname');
                $profile->position =  $request->input('position');
                $profile->created_at = date('Y-m-d H:i:s');
                $profile->updated_at = date('Y-m-d H:i:s');
                
                if($profile->save())
                {
                    
                    $u = new User;
                    $u->username = $pid;
                    $u->password = Hash::make($soldier_id);
                    $u->created_at = date('Y-m-d H:i:s');
                    $u->updated_at = date('Y-m-d H:i:s');
                    $u->save();
                    $c = User::where('username','admin')->count();
                        if ($c>0)
                        {
                          $c = User::where('username','admin')->delete();  
                          $result = array('DelAdmin'=>'1');
                          Auth::logout();
                        }else{
                          $result = array('success'=>true);
                        }

                }else{
                    $result = array('errorMsg'=>'ไม่สามารถบันทึกข้อมูลได้');
                } 
                return json_encode($result);
            break;
            case 'configsystem':
                $count_office = Office::count();
                    $day = $request->input('date_dob');
                    $month = $request->input('month_dob');
                    $year = $request->input('year_regis')-543;
                    $date = $year.'-'.$month.'-'.$day;
                if($count_office <=0)
                {
                    $office = new Office;
                    $office->office_name = $request->input('office_name');
                    $office->office_shortname = $request->input('office_shortname');
                    $office->office_no = $request->input('office_no');
                    $office->office_soldier_no = $request->input('office_soldier_no');
                    $office->road = $request->input('road');
                    $office->trok = $request->input('trok');
                    $office->soy = $request->input('soy');
                    $office->office_home_id = $request->input('office_home_id');
                    $office->office_moo = $request->input('office_moo');
                    $office->office_district = $request->input('office_district');
                    $office->office_amphur = $request->input('office_amphur');
                    $office->office_province = $request->input('office_province');
                    $office->office_tel = $request->input('office_tel');
                    $office->day_surv = $date;
                    $office->place_surv = $request->input('place');
                    $office->save();
                    $result = array('status'=>1);
                    return json_encode($result);
                }else{
                    Office::where('id','1')->update([
                    'office_name'=> $request->input('office_name'),
                    'office_shortname'=> $request->input('office_shortname'),
                    'office_no'=> $request->input('office_no'),
                    'office_soldier_no'=> $request->input('office_soldier_no'),
                    'road'=> $request->input('road'),
                    'trok'=> $request->input('trok'),
                    'soy'=> $request->input('soy'),
                    'office_home_id'=> $request->input('office_home_id'),
                    'office_moo'=> $request->input('office_moo'),
                    'office_district'=> $request->input('office_district'),
                    'office_amphur'=> $request->input('office_amphur'),
                    'office_province'=> $request->input('office_province'),
                    'office_tel'=> $request->input('office_tel'),
                    'day_surv' => $date,
                    'place_surv' => $request->input('place')

                        ]);
                    $result = array('status'=>1);
                    return json_encode($result);
                }
            break;
            case 'mooban':
            $tumbon = $request->input('district');
            $moo = $request->input('moo');
            $count = Mooban::where(array('district'=>$tumbon,'moo'=>$moo))->count();
                if($count<=0)
                {
                    $ban = new Mooban;
                    $ban->district =$request->input('district');
                    $ban->moo =$request->input('moo');
                    $ban->banname =$request->input('banname');
                    $ban->bossname =$request->input('bossname');
                    $ban->phonenumber =$request->input('phonenumber');
                    $ban->save();
                    $result = array('success'=>true,'errorMsg'=>'บันทึกข้อมูลเรียบร้อยแล้ว');
                    return json_encode($result);

                }else{
                    $result =array('status'=>0,'errorMsg'=>'มีข้อมูลหมู่บ้านนี้แล้ว ไม่ต้องบันทึกเพิ่ม');
                    return json_encode($result);
                }
               
                break;
            
            
    	endswitch;

    	}else{
    		return redirect('/');
    	}

    }

    public function postEdituser(Request $request,$id)
    {
        if(Auth::check()){
          
                $pid = str_replace('-','',$request->input('pid'));
                $soldier_id = str_replace('-','',$request->input('sol_id'));
                $profile = Profile::where('pid',$id)
                ->update([
                'pid' =>  $pid,
                'sol_id' =>  $soldier_id,
                'prefix' =>  $request->input('prefix'),
                'firstname' =>  $request->input('firstname'),
                'lastname' =>  $request->input('lastname'),
                'position' =>  $request->input('position'),
                'updated_at' => date('Y-m-d H:i:s')]);
                $u = User::where('username',$id)->update(['username'=>$pid,'password'=>Hash::make($soldier_id)]);
                return json_encode(array('success'=>true));
             
        }else{
            return redirect('/');
        }
    }
     public function postDeleteuser(Request $request)
    {
        
        if(Auth::check()){
         $id = $request->input('id');
         Profile::where('pid',$id)->delete();
         return json_encode(array('success'=>true));
        }else{
            return redirect('/');
        }
    }  
  public function postDeleteban(Request $request)
    {
        if(Auth::check()){
         $district = $request->input('id');
         $moo = $request->input('moo');
         Mooban::where(array('district'=>$district,'moo'=>$moo))->delete();
         return json_encode(array('success'=>true));
        }else{
            return redirect('/');
        }
    }  

    public function postEditban(Request $request,$district,$id)
    {
        if(Auth::check()){
          
               $ban = Mooban::where(array( 
                'district'=> $request->input('district'),
                'moo' => $request->input('moo')
                    ))->update([
                    'district'=> $request->input('district'),
                    'moo' => $request->input('moo'),
                    'banname' =>$request->input('banname'),
                    'bossname' =>$request->input('bossname'),
                    'phonenumber' => $request->input('phonenumber')

                    ]);
                return json_encode(array('success'=>true,'errorMsg'=>'แก้ไขข้อมูลเรียบร้อย!'));
             
        }else{
            return redirect('/');
        }
    }
    
    
}
