<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Sd1;
use Helpers;
use DB;
use App\District;
use App\Aumphur;
use App\Province;
use App\Position;
use App\Scar;
use App\Office;
use Excel;
Use PDF;

class ExportController extends Controller
{
   public function postExport($type, Request $request)
   {
    if(Auth::check())
      {
    switch ($type):
      case 'sd35year':
         $c = DB::table('tb_config_print_sd35')->first();
         $tumbon = DB::table('tb_district')->where('DISTRICT_CODE',$request->input('district'))->first();
          $year = $request->input('year_dob');
          $yy = $year - 543;
          $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_amphur.AMPHUR_NAME','tb_province.PROVINCE_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->join('tb_amphur','tb_amphur.AMPHUR_CODE','=','tb_sd1.amphur')
                  ->join('tb_province','tb_province.PROVINCE_CODE','=','tb_sd1.province')
                  ->where('tb_sd1.district','=',$request->input('district'))
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '".$yy."'" )
                   ->orderBy('sd1_no','asc')
                  ->get();
 
                
                //พ.ศ.เกิด
                $year_k = Helpers::ShortYear($request->input('year_regis')); //ปีที่ทำการตรวจเลือก
                $year_g = Helpers::ShortYear($request->input('year_regis')-1); //ปรที่รับหมายเรียก
                
                $age = Helpers::cthai('20'); // อายุ
                $A = $c->column_a;
                $B = $c->column_b;
                $G = $c->column_g;
                $row8 = $c->row8;$row10 = $c->row10;$row11=$c->row11;$row12=$c->row12;
                $row13=$c->row13;$row15=$c->row15;$row36=$c->row36;
                $day =  Helpers::cthai($request->input('date_dob')) ;
                $month =  Helpers::Cmonth($request->input('month_dob'));
                $place =  $request->input('place');
              
          Excel::create('หมายเรียก คนเกิด พ.ศ.'.$year.' ตำบล'.$tumbon->DISTRICT_NAME, function($excel) use ($A,$B,$G,$sql,$row8,$row10,$row11,$row12,$row13,$row15,$row36,$day,$month,$year_g,$year_k,$age,$place){
            $excel->sheet('แบบพิมพ์ สด.๔๔', function($sheet) use ($A,$B,$G,$sql,$row8,$row10,$row11,$row12,$row13,$row15,$row36,$day,$month,$year_g,$year_k,$age,$place) {
           // $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    )
                ));
                // Set top, right, bottom, left
                $sheet->setPageMargin(array(
                0.7520833333333334, 0.7043478260857316, 0.7520833333333334, 0.7043478260857316
                ));
                $sheet->setHeight(array(8 =>$row8,10=>$row10,11=>$row11,12=>$row12,13=>$row13,15=>$row15,36=>$row36));
                $sheet->setWidth(array(
                    'A'=> $A,
                    'B'=> $B,
                    'G'=> $G,
                    'C'=> 9.01,
                    'D'=> 9.01,
                    'E'=> 9.01,
                    'F'=> 9.01,
                    'H'=>9.01,
                    'I'=>9.01
                   ));
                

                  $sheet->cell('A1', function($cell) {
                  $cell->setAlignment('left');
                   });
                  $sheet->cell('A36', function($cell) {
                  $cell->setAlignment('center');
                   });
                   $sheet->cell('B11', function($cell) {
                  $cell->setAlignment('center');
                   });
                    $sheet->cell('C8', function($cell) {
                  $cell->setAlignment('center');
                   });
                      $sheet->cell('H8', function($cell) {
                  $cell->setAlignment('center');
                   });
                      $sheet->cell('H9', function($cell) {
                  $cell->setAlignment('right');
                   });
                      $sheet->cell('H10', function($cell) {
                  $cell->setAlignment('right');
                   });
                  $sheet->cell('G1', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G2', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G3', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G4', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G6', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('F12', function($cell) {
                  $cell->setFontColor('#ff000');
                   $cell->setAlignment('center');
                   });
                  $sheet->cell('G12', function($cell) {
                  $cell->setFontColor('#ff000');
                   $cell->setAlignment('right');
                   });
                  $sheet->cell('C14', function($cell) {
                  $cell->setFontColor('#ff000');
                  //$cell->setAlignment('right');
                   });
                   $sheet->cell('D16', function($cell) {
                  $cell->setFontColor('#ff000');
                  $cell->setAlignment('center');
                   });
                  
                // for($i=1;$i<=$totalrow;$i++){
                //   if ($i % 40 == 0) {
                //    $sheet->setBreak( 'A'.$i );
                //   }
                // }
                $sheet->loadView('export.sd35',array(
                'sql'=>$sql,
                'column_a'=>$A,
                'column_b'=>$B,
                'column_g'=>$G,
                'row8'=>$row8,
                'row10'=>$row10,
                'row11'=>$row11,
                'row12'=>$row12,
                'row13'=>$row13,
                'row15'=>$row15,
                'row36'=>$row36,
                'day'=>$day,
                'month'=>$month,
                'year_g'=>$year_g,
                'year_k'=>$year_k,
                'age'=>$age,
                'place'=>$place));
            });
 
          })->download('xls');
          
      break;
       case 'reportsd35year':
      
         $tumbon = DB::table('tb_district')->where('DISTRICT_CODE',$request->input('district'))->first();
          $year = $request->input('year_dob');
          $yy = $year - 543;
          $year_g = Helpers::ShortYear($request->input('year_regis')-1); //ปรที่รับหมายเรียก
          $sql = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_amphur.AMPHUR_NAME','tb_province.PROVINCE_NAME','tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section')
                  ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
                  ->join('tb_amphur','tb_amphur.AMPHUR_CODE','=','tb_sd1.amphur')
                  ->join('tb_province','tb_province.PROVINCE_CODE','=','tb_sd1.province')
                  ->where('tb_sd1.district','=',$request->input('district'))
              //    ->where(DB::raw("YEAR(dob) = '1997'"))
                   ->whereRaw("YEAR(dob) = '".$yy."'" )
                   ->orderBy('sd1_no','asc')
                  ->get();
          $tumbon_name = $tumbon->DISTRICT_NAME;
                
               
              
          Excel::create('งบหน้าหมายเรียก คนเกิด พ.ศ.'.$year.' ตำบล'.$tumbon->DISTRICT_NAME, function($excel) use ($sql,$tumbon_name,$year_g){
            $excel->sheet('แบบพิมพ์ สด.๔๔', function($sheet) use ($sql,$tumbon_name,$year_g) {
           // $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    )
                ));
                // Set top, right, bottom, left
                $sheet->setPageMargin(array(
                0.7520833333333334, 0.25, 0.7520833333333334, 0.25
                ));
                
     
 
                  
                // for($i=1;$i<=$totalrow;$i++){
                //   if ($i % 40 == 0) {
                //    $sheet->setBreak( 'A'.$i );
                //   }
                // }
                $sheet->loadView('export.reportsd35',array(
                'sql'=>$sql,
                'tumbon'=>$tumbon_name,'year_g'=>$year_g));
            });
 
          })->download('xls');
          
      break;
      case 'sd44':
      $data = [1,2,3,4,5];
      $pdf = PDF::loadView('test_pdf',$data);
      return $pdf->stream('test_pdf.pdf'); //แบบนี้จะ stream มา preview
      //return $pdf->download('test.pdf'); //แบบนี้จะดาวโหลดเลย
      break;
      case 'sd44_2':
          $id = str_replace('-','',$request->input('pid'));
          $year =$request->input('year_dob');
          $count = Sd1::where('pid',$id)->count();
          if($count <= 0)
          {
              return json_encode(array('status'=>0));
          }else{
            $sd1_size = DB::table('tb_config_print_sd1')->first();
            $sd44_size = DB::table('tb_config_print_sd44')->first();
            $sd35_size = DB::table('tb_config_print_sd35')->first();
            $sd9_size = DB::table('tb_config_print_sd9')->where('year','=',$year)->first();
          
          $s = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.sd9_book','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_province.PROVINCE_NAME'
            ,'tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section','tb_sd1.trok','tb_sd1.soy','tb_sd1.road'
            ,'tb_amphur.AMPHUR_NAME','tb_scar.scar_name','tb_zodiac.zodiac_name','tb_nation.nation_name','tb_religion.religion_name','tb_occupation.occupation_name'
            ,'tb_education.education_name','tb_sd1.fa_nationality','tb_sd1.pcard_issue','tb_sd1.pcard_by','tb_sd1.pcard_id')
            ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
            ->join('tb_province','tb_province.PROVINCE_CODE','=','tb_sd1.province')
            ->join('tb_amphur','tb_amphur.AMPHUR_CODE','=','tb_sd1.amphur')
            ->join('tb_scar','tb_scar.scar_no','=','tb_sd1.scar_id')
            ->join('tb_zodiac','tb_zodiac.zodiac_no','=','tb_sd1.zodiac_id')
            //->join('tb_nation','tb_nation.nation_no','=','tb_sd1.nationality_id')
            ->join('tb_nation','tb_nation.nation_no','=','tb_sd1.fa_nationality')
            ->join('tb_religion','tb_religion.religion_no','=','tb_sd1.religion_id')
            ->join('tb_occupation','tb_occupation.occupation_no','=','tb_sd1.occupation_id')
            ->join('tb_education','tb_education.education_no','=','tb_sd1.education_id')
            ->where('tb_sd1.pid','=',$id)
            ->first();
                // $data_tumbon = District::where('DISTRICT_CODE',$s->district)->first();
                // $data_amphur = Aumphur::where('AMPHUR_CODE',$s->amphur)->first();
                // $data_province = Province::where('PROVINCE_CODE',$s->province)->first();
                // $data_scar = Scar::where('scar_no',$s->scar_id)->first();
                // $tumbon = $data_tumbon->DISTRICT_NAME;
                // $amphur = $data_amphur->AMPHUR_NAME;
                // $province = $data_province->PROVINCE_NAME;
                // $scar = $data_scar->scar_name;
              
          Excel::create('ช้อมูลการลงบัญชีฯ นาย'.$s->firstname.' '.$s->lastname , function($excel) use (
            $s,$sd1_size,$sd9_size,$sd44_size,$sd35_size

            ){
            $excel->sheet('สด.44', function($sheet) use ($s,$sd44_size) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  18,
                    )
                ));
                $sheet->setHeight(array(1 =>$sd44_size->row1,10=>$sd44_size->row10));
                $sheet->setWidth(array(
                    'A'=> $sd44_size->column_a,
                    'B'=> $sd44_size->column_b,
                    'C'=> $sd44_size->column_c,
                    'D'=> $sd44_size->column_d,
                    'E'=> $sd44_size->column_e,
                    'F'=> $sd44_size->column_f,
                    'G'=> $sd44_size->column_g,
                    'H'=> $sd44_size->column_h,
                    'I'=> $sd44_size->column_i,
                    'J'=> $sd44_size->column_j,
                    'K'=> $sd44_size->column_k,
                    'L'=> $sd44_size->column_l,
                    'M'=> $sd44_size->column_m,
                    'N'=> $sd44_size->column_n

                    ));
                $sheet->cell('J2', function($cell) {
            $cell->setAlignment('left');
        });
              $sheet->cell('G3',function($cell){
            $cell->setAlignment('center');
        });
                $sheet->cell('N3',function($cell){
                    $cell->setAlignment('right');
                });
                $sheet->cell('N4',function($cell){
                    $cell->setAlignment('right');
                });
                $sheet->cell('F6',function($cell){
                    $cell->setAlignment('right');
                });
                $sheet->cell('F7',function($cell){
                    $cell->setAlignment('right');
                });
                $sheet->cell('F12',function($cell){
                    //$cell->setAlignment('right');
                    $cell->setFontSize(14);
                });
                $sheet->cell('M12',function($cell){
                    //$cell->setAlignment('right');
                    $cell->setFontSize(14);
                });
         
                $sheet->loadView('export.sd44',array('s'=>$s));




            });
            $excel->sheet('สด.1', function($sheet) use ($s,$sd1_size
              ) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    )
                ));
                $y = explode('-',$s->dob);
                $classes = ($y[0]+543)+18;
                $sheet->loadView('export.sd1',array('s'=>$s,'c'=>$sd1_size,'class'=>$classes));




            });
             $excel->sheet('สด.9', function($sheet) use ($s,$sd9_size
              ) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                     'bold'       =>  true
                    )
                ));
                $y = explode('-',$s->dob);
                $classes = ($y[0]+543)+18;
                $noon1 = ($y[0]+543)+30;
                $noon2 = ($y[0]+543)+40;
                $noon3 = ($y[0]+543)+46;
                switch($s->section):
                  case '16':
                    $day =Helpers::cthai('1');
                    $month = 'มกราคม';
                    $year = Helpers::cthai($classes);
                  break;
                  case '18':
                    $day =Helpers::Datewrite($s->date_regis);
                    $month = Helpers::Monthwrite($s->date_regis);
                    $year = Helpers::Yearwrite($s->date_regis);
                  break;
                endswitch;
                $sheet->loadView('export.sd9',array('s'=>$s,'c'=>$sd9_size,'class'=>$classes,'day'=>$day,'month'=>$month,'year'=>$year,'noon1'=>$noon1,'noon2'=>$noon2,'noon3'=>$noon3));
            });
                if($s->age > 17)
                {
            $excel->sheet('ส่งตัวดำเนินคดี', function($sheet) use ($s,$sd1_size
              ) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    'bold' => false
                    )
                ));
                $y = explode('-',$s->dob);
                $year_dob = $y[0]+543;
                $sheet->loadView('export.kadee',array('s'=>$s,'c'=>$sd1_size));




            });
                }else if($s->age >=20 && $s->age <=28) {
                   $excel->sheet('หมายเรียกฯ สด.35', function($sheet) use ($s,$sd35_size) {
           // $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    )
                ));
                // Set top, right, bottom, left
                $sheet->setPageMargin(array(
                0.7520833333333334, 0.7043478260857316, 0.7520833333333334, 0.7043478260857316
                ));
                $sheet->setHeight(array(8 =>$sd35_size->row8,10=>$sd35_size->row10,11=>$sd35_size->row11,12=>$sd35_size->row12,13=>$sd35_size->row13,15=>$sd35_size->row15,36=>$sd35_size->row36));
                $sheet->setWidth(array(
                    'A'=> $sd35_size->column_a,
                    'B'=> $sd35_size->column_b,
                    'G'=> $sd35_size->column_g,
                    'C'=> 9.01,
                    'D'=> 9.01,
                    'E'=> 9.01,
                    'F'=> 9.01,
                    'H'=>9.01,
                    'I'=>9.01
                   ));
                

                  $sheet->cell('A1', function($cell) {
                  $cell->setAlignment('left');
                   });
                  $sheet->cell('A36', function($cell) {
                  $cell->setAlignment('center');
                   });
                   $sheet->cell('B11', function($cell) {
                  $cell->setAlignment('center');
                   });
                    $sheet->cell('C8', function($cell) {
                  $cell->setAlignment('center');
                   });
                      $sheet->cell('H8', function($cell) {
                  $cell->setAlignment('center');
                   });
                      $sheet->cell('H9', function($cell) {
                  $cell->setAlignment('right');
                   });
                      $sheet->cell('H10', function($cell) {
                  $cell->setAlignment('right');
                   });
                  $sheet->cell('G1', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G2', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G3', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G4', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('G6', function($cell) {
                  $cell->setFontColor('#ff000');
                   });
                  $sheet->cell('F12', function($cell) {
                  $cell->setFontColor('#ff000');
                   $cell->setAlignment('center');
                   });
                  $sheet->cell('G12', function($cell) {
                  $cell->setFontColor('#ff000');
                   $cell->setAlignment('right');
                   });
                  $sheet->cell('C14', function($cell) {
                  $cell->setFontColor('#ff000');
                  //$cell->setAlignment('right');
                   });
                   $sheet->cell('D16', function($cell) {
                  $cell->setFontColor('#ff000');
                  $cell->setAlignment('center');
                   });
                  
                // for($i=1;$i<=$totalrow;$i++){
                //   if ($i % 40 == 0) {
                //    $sheet->setBreak( 'A'.$i );
                //   }
                // }
                $sheet->loadView('export.sd35_1',array(
                's'=>$s,
                'column_a'=>$sd35_size->column_a,
                'column_b'=>$sd35_size->column_b,
                'column_g'=>$sd35_size->column_g,
                'row8'=>$sd35_size->row8,
                'row10'=>$sd35_size->row10,
                'row11'=>$sd35_size->row11,
                'row12'=>$sd35_size->row12,
                'row13'=>$sd35_size->row13,
                'row15'=>$sd35_size->row15,
                'row36'=>$sd35_size->row36,
                // 'day'=>$day,
                // 'month'=>$month,
                // 'year_g'=>$year_g,
                // 'year_k'=>$year_k,
                // 'age'=>$age,
                // 'place'=>$place
                ));
            });
 
                }


          })->download('xls');
        }
        break;
        case 'sd2':
          $district = $request->input('district');
          $tumbon = DB::table('tb_district')->where('DISTRICT_CODE',$district)->first();
           $amphur = Office::select('office_amphur')->first();
           $prov = Office::select('office_province')->first();
           $amphur_name = Aumphur::select('AMPHUR_CODE','AMPHUR_NAME')->where('AMPHUR_ID',$amphur->office_amphur)->first();
           $province_name =Province::select('PROVINCE_CODE','PROVINCE_NAME')->where('PROVINCE_ID',$prov->office_province)->first();
          $year = $request->input('year_dob');
          $yy = $year - 543;
          $class = $year + 18;
          $s = Sd1::select('tb_sd1.date_regis','tb_sd1.sd1_no','tb_sd1.sd9_no','tb_sd1.sd9_book','tb_sd1.pid','tb_sd1.firstname','tb_sd1.lastname','tb_sd1.dob','tb_sd1.age','tb_sd1.home_id','tb_sd1.moo','tb_district.DISTRICT_NAME','tb_province.PROVINCE_NAME'
            ,'tb_sd1.fa_name','tb_sd1.fa_lname','tb_sd1.ma_name','tb_sd1.ma_lname','tb_sd1.section','tb_sd1.trok','tb_sd1.soy','tb_sd1.road'
            ,'tb_amphur.AMPHUR_NAME','tb_scar.scar_name','tb_zodiac.zodiac_name','tb_nation.nation_name','tb_religion.religion_name','tb_occupation.occupation_name'
            ,'tb_education.education_name','tb_sd1.fa_nationality','tb_sd1.pcard_issue','tb_sd1.pcard_by','tb_sd1.pcard_id')
            ->join('tb_district','tb_district.DISTRICT_CODE','=','tb_sd1.district')
            ->join('tb_province','tb_province.PROVINCE_CODE','=','tb_sd1.province')
            ->join('tb_amphur','tb_amphur.AMPHUR_CODE','=','tb_sd1.amphur')
            ->join('tb_scar','tb_scar.scar_no','=','tb_sd1.scar_id')
            ->join('tb_zodiac','tb_zodiac.zodiac_no','=','tb_sd1.zodiac_id')
            //->join('tb_nation','tb_nation.nation_no','=','tb_sd1.nationality_id')
            ->join('tb_nation','tb_nation.nation_no','=','tb_sd1.fa_nationality')
            ->join('tb_religion','tb_religion.religion_no','=','tb_sd1.religion_id')
            ->join('tb_occupation','tb_occupation.occupation_no','=','tb_sd1.occupation_id')
            ->join('tb_education','tb_education.education_no','=','tb_sd1.education_id')
            ->where('tb_sd1.district','=',$district)
            ->where('tb_sd1.section','=','16')
            ->whereRaw("YEAR(dob) = '".$yy."'" )
            ->orderBy('sd1_no','asc')
            ->get();
             Excel::create('สด.๒ คนเกิด พ.ศ.'.$year.' ชั้นปี '.$class.' ตำบล'.$tumbon->DISTRICT_NAME, function($excel) use ($s,$class,$amphur_name,$province_name){
            $excel->sheet('แบบพิมพ์ สด.๒', function($sheet) use ($s,$class,$amphur_name,$province_name) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
            $sheet->setOrientation('landscape');
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  14,
                    'bold' => true
                    )
                ));
           
                $day = '๑ ม.ค.'.Helpers::ShortYear($class);
                $sheet->loadView('export.sd2',array(
                'sql'=>$s,'day'=>$day,'prov'=>$province_name,'amphur'=>$amphur_name,'class'=>$class));
            });
             $excel->sheet('ข้อมูล สด.๒', function($sheet) use ($s,$class,$amphur_name,$province_name) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
            $sheet->setOrientation('landscape');
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  16,
                    'bold' => true
                    )
                ));
           
                $day = '๑ ม.ค.'.Helpers::ShortYear($class);
                $sheet->loadView('export.datasd2',array(
                'sql'=>$s,'day'=>$day,'prov'=>$province_name,'amphur'=>$amphur_name,'class'=>$class));
            });
 
          })->download('xls');

        break;
        case 'sd2_m18':
          // $district = $request->input('district');
          // $tumbon = DB::table('tb_district')->where('DISTRICT_CODE',$district)->first();
            $year = $request->input('year_dob');
           $month = $request->input('month');
          $yy = $year - 543;
          $amphur = Office::select('office_amphur')->first();
           $prov = Office::select('office_province')->first();
           $amphur_name = Aumphur::select('AMPHUR_CODE','AMPHUR_NAME')->where('AMPHUR_ID',$amphur->office_amphur)->first();
           $province_name =Province::select('PROVINCE_CODE','PROVINCE_NAME')->where('PROVINCE_ID',$prov->office_province)->first();
         
          $year_regis = $request->input('year_regis');

         
         
          $class = $year + 18;
          
            $count_year = DB::table('tb_sd1')
          //  ->select('YEAR(dob)')
            ->select(DB::raw('YEAR(dob) as y'))
            ->whereRaw("YEAR(date_regis) = '".$yy."'" )
            ->whereRaw("MONTH(date_regis) = '".$month."'" )
            ->where('section','18')
            ->groupBy('y')
            ->orderBy('y','asc')
            ->get();
            // ("SELECT year(dob) as y FROM tb_sd1 where month(date_regis)='".$month."' and year(date_regis)='".$year."' and section='18' group by y")->get();
            // $count_year= Sd1::select("YEAR(dob) as y")
            // ->whereRaw("YEAR(date_regis) = '".$yy."'" )
            // ->whereRaw("MONTH(date_regis) = '".$month."'" )
            // ->where('section','18')
            // ->get();
             Excel::create('สด.๒ มาตรา ๑๘ ประจำเดือน '.Helpers::CmonthShort($month).' ปี '.$year, function($excel) use ($class,$amphur_name,$province_name,$count_year,$year_regis,$month){
            foreach ($count_year as $countdata =>$year_c){
              
             $sheet_year = $year_c->y+543;
            $excel->sheet('พ.ศ.'.$sheet_year, function($sheet) use ($s,$class,$amphur_name,$province_name) {
            $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
            $sheet->setOrientation('landscape');
                $sheet->setStyle(array(
                    'font' => array(
                    'name' =>  'TH SarabunPSK',
                    'size' =>  14,
                    'bold' => true
                    )
                ));
           
             
                $sheet->loadView('export.sd2_m18',array(
                'sql'=>$s,'prov'=>$province_name,'amphur'=>$amphur_name,'class'=>date('Y')+543));
            });
            }


 
          })->download('xls');

        break;
    endswitch;
    }else{
        return redirect('/');
      }
   }
}
