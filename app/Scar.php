<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scar extends Model
{
    protected $table = 'tb_scar';
    public $timestamps = false;
}
