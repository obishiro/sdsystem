<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thaiperson extends Model
{
    protected $table = 'tb_data0';
    public $timestamps = false;
}
