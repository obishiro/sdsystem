<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zodiac extends Model
{
    protected $table = 'tb_zodiac';
    public $timestamps = false;
}
