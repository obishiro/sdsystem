<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mooban extends Model
{
    protected $table = 'tb_mooban';
    public $timestamps = false;
}
