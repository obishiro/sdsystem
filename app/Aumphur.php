<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aumphur extends Model
{
    protected $table = 'tb_amphur';
    public $timestamps = false;
}
