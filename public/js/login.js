
$('#win').window({
        title:'เข้าสู่ระบบสารสนเทศสายงานสัสดี',
        width:320,
        height:360,
        modal:true,
        minimizable:false,
        maximizable:false,
        closable:false,
        collapsible:false,
        footer:"#footer",
        resizable:false

    });
$('#txt_name').validatebox({
        required: true,
        
    });
$('#txt_name').textbox({
        width: 270,
        iconCls:'icon-man',
        required:true,
        prompt:"รหัสผู้ใช้งาน"
        
});
$('#txt_password').passwordbox({
        width: 270,
        required:true,
        prompt:"รหัสผ่าน",
        showEye: true
});

 function submitForm(){
     
    // alert($('#form_login').serialize());

   $('#form_login').form('submit',{
        url: URL+'/login',
        cache:false,
        data:$('#form_login').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
      
        },
        success:function(data){
          
            if(data==1)
            {
                // $.messager.alert('ผลการทำงาน', "", 'success');
                window.location.href=URL+'/main'; 
            }else{
                $.messager.confirm('ผลการทำงาน', 'ไม่สามารถเข้าสู่ระบบได้', function(r){
                    if (r){
                     $('#form_login').form('clear');
                        window.location.href=URL+'/';
                        }
                        });
    
                
            }
        }
    });
    }

function clearForm()
    {
        $('#form_login').form('clear');
    }
    