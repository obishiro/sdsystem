function formsd44 () {
      $('#form_sd44').form('reset');
      
	    $('#main_layout').panel({
         href:URL+'/form/sd44',
         queryParams:{"pid":''}
})
	}

function formsd1 () {
      $('#main_layout').panel({
        href:URL+'/showreport/sd1'
})
  }
function formsortsd1 () {
      $('#main_layout').panel({
        href:URL+'/form/sortsd1'
})
  }
function formsd1_m16 () {
      $('#main_layout').panel({
        href:URL+'/showreport/sd1_m16'
})
  }
function formsd1_m18 () {
      $('#main_layout').panel({
        href:URL+'/showreport/sd1_m18'
})
  }

  function formsd35year () {
      $('#main_layout').panel({
        href:URL+'/form/sd35year'
})
  }
  function datathaiperson() {
    $('#form_thaiperson').form('clear');
    $('#main_layout').panel({

      href:URL+'/form/thaiperson'
    })
    
  }
    function formthaiperson() {
   // $('#form_thaiperson').form('clear');
    $('#main_layout').panel({

      href:URL+'/form/formthaiperson'
    })
    
  }



function saveformsd44() {
 		 $('#form_sd44').form('submit',{
        	url: URL+'/save/sd44',
        	cache:false,
      		data:$('#form_sd44').serialize(),
        onSubmit: function(){
        return $(this).form('validate');
            },
        success:function(data){
        	var obj = jQuery.parseJSON(data);
            if(obj.status==1)
            {
                $.messager.alert('ผลการทำงาน', "บันทึกข้อมูลการลงบัญชีทหารกองเกินเรียบร้อยแล้ว!", 'info');
                // window.location.href=URL+'/main'; 
                 $('#btn_printsd44').linkbutton({disabled: false});
                 $('#btn_printsd9').linkbutton({disabled: false});
                 $('#btn_printsd1').linkbutton({disabled: false});
                  // $('#main_layout').panel({
                  //    href:URL+'/form/sd44'
                  //   });

            }else if(obj.status==2){
                $.messager.alert('ผลการทำงาน', "บุคคลผู้นี้ได้แสดงตนเพื่อลงบัญชีทหารแล้ว", 'error');
                    $('#main_layout').panel({
                     href:URL+'/form/sd44'
                    });

            }
            if(obj.age>17 && obj.age <=19) {
            	$('#btn_printsendbook').linkbutton({disabled: false});
            }
            if(obj.age >=20) {
            	$('#btn_printsendbook').linkbutton({disabled: false});
            	$('#btn_printsd35').linkbutton({disabled: false});
            }
          
           
        }
    });
}

function clearformsd44() {
	$.messager.confirm('ผลการทำงาน', 'ต้องการล้างข้อมูลจริงหรือไม่?', function(r){
    if (r){
       $('#form_sd44').form('clear');
    }
	});
	
}

function clearformconfigsystem() {
  $.messager.confirm('ผลการทำงาน', 'ต้องการล้างข้อมูลจริงหรือไม่?', function(r){
    if (r){
       $('#form_configsystem').form('clear');
    }
  });
  
}
 